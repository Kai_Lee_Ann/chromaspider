﻿using System.Text;
using Persistence;
using TMPro;
using UnityEngine;

namespace Menu
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class LeaderboardService : MonoBehaviour
    {
        private const string Header = "# /Name          /Turns /Score";

        public LeaderboardData leaderboardData;
        private TextMeshProUGUI _leaderboard;

        private void Awake()
        {
            _leaderboard = GetComponent<TextMeshProUGUI>();
            UpdateLeaderBoard();
        }

        private void UpdateLeaderBoard()
        {
            var stringBuilder = new StringBuilder();
            stringBuilder.AppendLine(Header);
            for (int place = 0; place < leaderboardData.Entries.Count; place++)
            {
                var entry = leaderboardData.Entries[place];
                var entryPlace = (place + 1).ToString().PadRight(2);
                var nickname = entry.Nickname.PadRight(14);
                var entryTurns = entry.Turns.ToString().PadRight(6);
                var entryScore = entry.Score.ToString().PadRight(6);
                stringBuilder.AppendLine($"{entryPlace}/{nickname}/{entryTurns}/{entryScore}");
            }

            _leaderboard.text = stringBuilder.ToString();
        }
    }
}