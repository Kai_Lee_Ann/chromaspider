﻿using System;
using UnityEngine;

namespace Menu
{
    [ExecuteInEditMode]
    public class UiBuildSwitcher : MonoBehaviour
    {
        public GameObject desktopUi;
        public GameObject mobileUi;

        private void Awake()
        {
            bool desktopBuild = false;
            bool mobileBuild = false;
            
            #if UNITY_ANDROID
                mobileBuild = true;
            #elif UNITY_IPHONE
                mobileBuild = true;
            #elif UNITY_STANDALONE_WIN
                desktopBuild = true;
            #elif UNITY_WEBGL
                desktopBuild = true;
            #endif

            if (desktopBuild)
            {
                desktopUi.SetActive(true);
                mobileUi.SetActive(false);
            } else if (mobileBuild)
            {
                desktopUi.SetActive(false);
                mobileUi.SetActive(true);
            }
            else
            {
                desktopUi.SetActive(true);
                mobileUi.SetActive(false);
                throw new ArgumentException("Unknown build type!");
            }
        }
    }
}