﻿using System;
using Core.Service.Audio;
using Persistence;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Menu
{
    public class OptionsService : MonoBehaviour
    {
        private const float DefaultVolume = 0.75f;

        public PlayerSettings playerSettings;
        public Slider volumeSlider;
        [FormerlySerializedAs("muteToggle")] public Toggle soundsMuteToggle;
        public Toggle autoHintToggle;
        [FormerlySerializedAs("musicToggle")] public Toggle musicMuteToggle;

        private AudioSource _audioSource;
        private MusicService _musicService;

        private void OnEnable()
        {
            _audioSource = GetComponent<AudioSource>();
            _musicService = FindObjectOfType<MusicService>();

            autoHintToggle.isOn = playerSettings.AutoHintsEnabled;
            musicMuteToggle.isOn = !playerSettings.MusicEnabled;
            volumeSlider.value = playerSettings.SoundsVolume;
            soundsMuteToggle.isOn = AudioListener.volume == 0f;
        }

        public void UpdatePrefs()
        {
            playerSettings.SoundsVolume = volumeSlider.value;
            playerSettings.AutoHintsEnabled = autoHintToggle.isOn;
            playerSettings.MusicEnabled = !musicMuteToggle.isOn;
            playerSettings.Save();
        }

        public void UpdateVolume()
        {
            if (Math.Abs(AudioListener.volume - volumeSlider.value) < 0.01f)
            {
                return;
            }

            AudioListener.volume = volumeSlider.value;
            if (!_audioSource.isPlaying)
            {
                _audioSource.Play();
            }

            soundsMuteToggle.SetIsOnWithoutNotify(volumeSlider.value == 0);
        }

        public void ToggleSound()
        {
            if (soundsMuteToggle.isOn)
            {
                AudioListener.volume = 0;
                volumeSlider.value = 0;
                return;
            }

            volumeSlider.value = DefaultVolume;
            AudioListener.volume = volumeSlider.value;
        }

        public void ToggleMusic()
        {
            if (_musicService == null)
            {
                return;
            }
            _musicService.Enable(!musicMuteToggle.isOn);
        }

        private void OnDestroy()
        {
            UpdatePrefs();
        }
    }
}