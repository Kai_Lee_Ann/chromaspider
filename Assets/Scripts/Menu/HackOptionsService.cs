﻿using Core.Settings;
using Persistence;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Menu
{
    public class HackOptionsService : MonoBehaviour
    {
        public GameSettings hackGameSettings;
        public HackModeData hackModeData;
        public TextMeshProUGUI boardSizeLabel;
        public Slider boardSizeSlider;
        public TextMeshProUGUI colorsCountLabel;
        public Slider colorsCountSlider;
        public TextMeshProUGUI minimalMatchLengthLabel;
        public Slider minimalMatchLengthSlider;
        public TextMeshProUGUI minimalSpecialMatchLengthLabel;
        public Slider minimalSpecialMatchLengthSlider;
        public TMP_InputField scoreToWinInputField;
        public TMP_InputField turnsInputField;

        private void Awake()
        {
            boardSizeSlider.onValueChanged.AddListener(value => OnBoardSizeChanged((int) value));
            colorsCountSlider.onValueChanged.AddListener(value => OnColorsCountChanged((int) value));
            minimalMatchLengthSlider.onValueChanged.AddListener(value => OnMinimalMatchLengthChanged((int) value));
            minimalSpecialMatchLengthSlider.onValueChanged.AddListener(value => OnMinimalSpecialMatchLength((int) value));
            scoreToWinInputField.onValueChanged.AddListener(OnTargetScoreValueChanged);
            turnsInputField.onValueChanged.AddListener(OnTurnsValueChanged);
            RestoreData();
        }

        private void RestoreData()
        {
            hackGameSettings.turns = hackModeData.Data.Turns;
            hackGameSettings.colorCount = hackModeData.Data.ColorsCount;
            hackGameSettings.columnSize = hackModeData.Data.BoardSize;
            hackGameSettings.rowSize = hackModeData.Data.BoardSize;
            hackGameSettings.targetScore = hackModeData.Data.TargetScore;
            hackGameSettings.minMatchLength = hackModeData.Data.MinMatchLength;
            hackGameSettings.minSpecialMatchLength = hackModeData.Data.MinSpecialMatchLength;

            boardSizeSlider.value = hackGameSettings.columnSize;
            colorsCountSlider.value = hackGameSettings.colorCount;
            minimalMatchLengthSlider.value = hackGameSettings.minMatchLength;
            minimalSpecialMatchLengthSlider.value = hackGameSettings.minSpecialMatchLength;
            scoreToWinInputField.text = hackGameSettings.targetScore.ToString();
            turnsInputField.text = hackGameSettings.turns.ToString();
        }

        public void SaveData()
        {
            hackModeData.Data.Turns = hackGameSettings.turns;
            hackModeData.Data.ColorsCount = hackGameSettings.colorCount;
            hackModeData.Data.BoardSize = hackGameSettings.columnSize;
            hackModeData.Data.BoardSize = hackGameSettings.rowSize;
            hackModeData.Data.TargetScore = hackGameSettings.targetScore;
            hackModeData.Data.MinMatchLength = hackGameSettings.minMatchLength;
            hackModeData.Data.MinSpecialMatchLength = hackGameSettings.minSpecialMatchLength;
            hackModeData.SavePrefs();
        }

        private void OnTurnsValueChanged(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                hackGameSettings.turns = 0;
                turnsInputField.SetTextWithoutNotify("");
                return;
            }

            hackGameSettings.turns = int.TryParse(value, out int result) ? result : int.MaxValue;
            turnsInputField.SetTextWithoutNotify(hackGameSettings.turns.ToString());
        }

        private void OnTargetScoreValueChanged(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                hackGameSettings.targetScore = 0;
                scoreToWinInputField.SetTextWithoutNotify("");
                return;
            }

            hackGameSettings.targetScore = int.TryParse(value, out int result) ? result : int.MaxValue;
            scoreToWinInputField.SetTextWithoutNotify(hackGameSettings.targetScore.ToString());
        }

        private void OnMinimalSpecialMatchLength(int minimalSpecialMatchLength)
        {
            hackGameSettings.minSpecialMatchLength = minimalSpecialMatchLength;
            var size = hackGameSettings.rowSize;
            if (hackGameSettings.minSpecialMatchLength > size)
            {
                minimalSpecialMatchLengthSlider.SetValueWithoutNotify(size);
                hackGameSettings.minSpecialMatchLength = size;
            }

            if (hackGameSettings.minSpecialMatchLength < hackGameSettings.minMatchLength)
            {
                minimalSpecialMatchLengthSlider.SetValueWithoutNotify(hackGameSettings.minMatchLength);
                hackGameSettings.minSpecialMatchLength = size;
            }

            minimalSpecialMatchLengthLabel.text = $"Minimal Special Match Length: {hackGameSettings.minSpecialMatchLength.ToString()}";
        }

        private void OnMinimalMatchLengthChanged(int minMatchLength)
        {
            hackGameSettings.minMatchLength = minMatchLength;
            var size = hackGameSettings.rowSize;
            if (hackGameSettings.minMatchLength > size)
            {
                minimalMatchLengthSlider.SetValueWithoutNotify(size);
                hackGameSettings.minMatchLength = size;
            }

            minimalSpecialMatchLengthSlider.minValue = hackGameSettings.minMatchLength;

            minimalMatchLengthLabel.text = $"Minimal Match Length: {hackGameSettings.minMatchLength.ToString()}";
        }

        private void OnColorsCountChanged(int colorsCount)
        {
            hackGameSettings.colorCount = colorsCount;
            colorsCountLabel.text = $"Colors Count: {hackGameSettings.colorCount.ToString()}";
        }

        private void OnBoardSizeChanged(int size)
        {
            hackGameSettings.rowSize = size;
            hackGameSettings.columnSize = size;
            minimalMatchLengthSlider.maxValue = size;
            minimalSpecialMatchLengthSlider.maxValue = size;
            boardSizeLabel.text = $"Board Size: {hackGameSettings.rowSize.ToString()} x {hackGameSettings.rowSize.ToString()}";
        }
    }
}