﻿using System;
using Core.Service.Audio;
using UnityEngine;

namespace Persistence
{
    [CreateAssetMenu(fileName = "PlayerSettings", menuName = "Custom Scriptable Objects/Player Settings")]
    public class PlayerSettings : ScriptableObject
    {
        private const string SoundsVolumeKey = "MusicVolume";
        private const string AutoHintKey = "AutoHints";
        private const string MusicEnabledKey = "MusicEnabled";

        public float SoundsVolume { get; set; }
        public bool AutoHintsEnabled { get; set; }
        public bool MusicEnabled { get; set; }

        public void Start()
        {
            SoundsVolume = PlayerPrefs.HasKey(SoundsVolumeKey) 
                ? PlayerPrefs.GetFloat(SoundsVolumeKey)
                : AudioListener.volume;
            AutoHintsEnabled = !PlayerPrefs.HasKey(AutoHintKey) || PlayerPrefs.GetInt(AutoHintKey) > 0;
            AudioListener.volume = SoundsVolume;
            MusicEnabled = !PlayerPrefs.HasKey(MusicEnabledKey) || PlayerPrefs.GetInt(MusicEnabledKey) > 0;
            FindObjectOfType<MusicService>()?.Enable(MusicEnabled);
        }

        public void Save()
        {
            PlayerPrefs.SetFloat(SoundsVolumeKey, SoundsVolume);
            PlayerPrefs.SetInt(AutoHintKey, Convert.ToInt32(AutoHintsEnabled));
            PlayerPrefs.SetInt(MusicEnabledKey, Convert.ToInt32(MusicEnabled));
            PlayerPrefs.Save();
        }
    }
}