﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using UnityEngine;

namespace Persistence
{
    [CreateAssetMenu(fileName = "LeaderboardData", menuName = "Custom Scriptable Objects/Leaderboard Data")]
    public class LeaderboardData : ScriptableObject
    {
        private const string LeaderboardPrefsKey = "Leaderboard";
        private const string NamelessPlayerName = "Shy One";

        private readonly IList<Entry> _defaultEntryList = new List<Entry>
        {
            new Entry("I. Asimov", 18, 8000),
            new Entry("K. Bulychev", 19, 7750),
            new Entry("P. K. Dick", 20, 7500),
            new Entry("R. Bradbury", 21, 7350),
            new Entry("A. Strugatsky", 22, 7250),
            new Entry("G. Orwell", 23, 7250),
            new Entry("A. Huxley", 24, 7250),
            new Entry("A. Belyaev", 25, 7250),
            new Entry("A. Tolstoy", 26, 7250),
            new Entry("S. Snegov", 27, 7250),
        };

        public IList<Entry> Entries { get; private set; }

        private void OnEnable()
        {
            if (PlayerPrefs.HasKey(LeaderboardPrefsKey))
            {
                Entries = Deserialize(PlayerPrefs.GetString(LeaderboardPrefsKey));
                return;
            }

            Entries = _defaultEntryList;
            SavePrefs();
        }

        public bool IsNewRecord(int turns, int score)
        {
            return Entries.Any(entry => turns == entry.Turns && score > entry.Score || turns < entry.Turns);
        }

        public void AddEntry(string playerName, int turns, int score)
        {
            for (var place = 0; place < Entries.Count; place++)
            {
                var entry = Entries[place];
                if ((turns != entry.Turns || score <= entry.Score) && turns >= entry.Turns)
                {
                    continue;
                }
                var insertName = string.IsNullOrEmpty(playerName) ? NamelessPlayerName : playerName;
                Entries.Insert(place, new Entry(insertName, turns, score));
                break;
            }
            Entries.Remove(Entries.Last());
            SavePrefs();
        }

        private void SavePrefs()
        {
            PlayerPrefs.SetString(LeaderboardPrefsKey, Serialize(Entries));
            PlayerPrefs.Save();
        }

        private static IList<Entry> Deserialize(string xmlString)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(List<Entry>));
            using (var stringReader = new StringReader(xmlString))
            {
                object list = xmlSerializer.Deserialize(stringReader);
                return (IList<Entry>) list;
            }
        }

        private static string Serialize(IList<Entry> entries)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(entries.GetType());
            using (StringWriter textWriter = new StringWriter())
            {
                xmlSerializer.Serialize(textWriter, entries);
                return textWriter.ToString();
            }
        }

        // Should be public for serialization 
        public class Entry
        {
            // Default constructor for serialization 
            public Entry()
            {
            }

            public Entry(string nickname, int turns, int score)
            {
                Turns = turns;
                Score = score;
                Nickname = nickname;
            }

            public string Nickname { get; set; } // Set property for serialization 

            public int Turns { get; set; } // Set property for serialization

            public int Score { get; set; } // Set property for serialization
        }
    }
}