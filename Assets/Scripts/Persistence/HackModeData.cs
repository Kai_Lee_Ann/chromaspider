﻿using System.IO;
using System.Xml.Serialization;
using Core.Settings;
using UnityEngine;

namespace Persistence
{
    [CreateAssetMenu(fileName = HackModeDataKey, menuName = "Custom Scriptable Objects/Hack Mode Data")]
    public class HackModeData : ScriptableObject
    {
        private const string HackModeDataKey = "HackModeData";
        public GameSettings defaultGameSettings;
        public GameSettingsEntry Data { get; private set; }

        public void OnEnable()
        {
            if (PlayerPrefs.HasKey(HackModeDataKey))
            {
                Data = Deserialize(PlayerPrefs.GetString(HackModeDataKey));
                return;
            }
            Data = new GameSettingsEntry
            {
                BoardSize = defaultGameSettings.rowSize,
                ColorsCount = defaultGameSettings.colorCount,
                MinMatchLength =  defaultGameSettings.minMatchLength,
                MinSpecialMatchLength =  defaultGameSettings.minSpecialMatchLength,
                TargetScore = defaultGameSettings.targetScore,
                Turns = defaultGameSettings.turns
            };
            SavePrefs();
        }

        private static GameSettingsEntry Deserialize(string xmlString)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(GameSettingsEntry));
            using (var stringReader = new StringReader(xmlString))
            {
                object list = xmlSerializer.Deserialize(stringReader);
                return (GameSettingsEntry) list;
            }
        }

        private static string Serialize(GameSettingsEntry entries)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(entries.GetType());
            using (StringWriter textWriter = new StringWriter())
            {
                xmlSerializer.Serialize(textWriter, entries);
                return textWriter.ToString();
            }
        }
        
        public void SavePrefs()
        {
            PlayerPrefs.SetString(HackModeDataKey, Serialize(Data));
            PlayerPrefs.Save();
        }

        public class GameSettingsEntry
        {
            public int BoardSize { get; set; }
            public int ColorsCount { get; set; }
            public int MinMatchLength { get; set; }
            public int MinSpecialMatchLength { get; set; }
            public int TargetScore { get; set; }
            public int Turns { get; set; }
        }

    }
}