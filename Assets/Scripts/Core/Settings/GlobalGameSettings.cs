﻿using Persistence;
using UnityEngine;

namespace Core.Settings
{
    public class GlobalGameSettings : MonoBehaviour
    {
        public PlayerSettings playerSettings;

        private void OnEnable()
        {
            Application.targetFrameRate = 60;
        }

        private void Start()
        {
            // Hack to load player settings early on
            playerSettings.Start();
        }
    }
}