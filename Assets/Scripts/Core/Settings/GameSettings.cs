﻿using UnityEngine;

namespace Core.Settings
{
    [CreateAssetMenu(fileName = "GameSettings", menuName = "Custom Scriptable Objects/Game Settings")]
    public class GameSettings : ScriptableObject
    {
        public int columnSize;
        public int rowSize;
        public int colorCount;
        public int minMatchLength;
        public int minSpecialMatchLength;
        public int cellScorePrice;
        public int turns;
        public int startingScore;
        public int targetScore;
        public int maxCombo;
    }
}