﻿using System.Collections.Generic;
using System.Linq;
using Core.Board.Model;
using Core.Board.View;
using Core.Domain;

namespace Core.Board.Presenter.SpecialEffect
{
    public class DestroyColumn : ISpecialEffect
    {
        public SpecialEffectName Name => SpecialEffectName.DestroyColumn;

        public IEnumerable<Cell> GetAffectedCells(AbstractBoardModel board, BoardPoint boardPoint)
        {
            return board.GetColumn(boardPoint.Column);
        }

        public void ApplyEffect(AbstractBoardModel board, AbstractBoardView view, Cell specialCell)
        {
            var column = GetAffectedCells(board, specialCell.BoardPoint).ToList();
            foreach (Cell cell in column)
            {
                if (cell.IsSpecial() && !cell.Equals(specialCell))
                {
                    continue;
                }
                board.RemoveCell(cell.BoardPoint);
                view.RemoveCell(cell.BoardPoint);
                view.EmitDestroyParticles(cell.BoardPoint, cell.Color.GetColor());
            }
            view.EmitDestroySpecialParticles(specialCell.BoardPoint, specialCell.Color.GetColor()); 
        }
    }
}