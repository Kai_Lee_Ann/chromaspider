﻿using System;
using System.Collections.Generic;
using Core.Domain;
using Core.Settings;
using UnityEngine;

namespace Core.Board.Presenter.SpecialEffect
{
    public class SpecialEffectFactory : MonoBehaviour
    {
        public GameSettings gameSettings;

        private DestroyRow _destroyRow;
        private DestroyColumn _destroyColumn;
        private DestroyNeighbors _destroyNeighbors;
        private int _gameSettingsMinSpecialMatchLength;

        private void Awake()
        {
            _destroyRow = new DestroyRow();
            _destroyColumn = new DestroyColumn();
            _destroyNeighbors = new DestroyNeighbors();
            _gameSettingsMinSpecialMatchLength = gameSettings.minSpecialMatchLength;
        }

        public ISpecialEffect GetSpecialEffect(ICollection<Cell> cells, MatchShape shape)
        {
            if (cells.Count < _gameSettingsMinSpecialMatchLength)
            {
                return null;
            }

            switch (shape)
            {
                case MatchShape.Column:
                    return _destroyColumn;
                case MatchShape.Row:
                    return _destroyRow;
                case MatchShape.Cross:
                    return _destroyNeighbors;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
        
        public ISpecialEffect GetSpecialEffect(SpecialEffectName specialEffectName)
        {
            switch (specialEffectName)
            {
                case SpecialEffectName.None:
                    return null;
                case SpecialEffectName.DestroyRow:
                    return _destroyRow;
                case SpecialEffectName.DestroyColumn:
                    return _destroyColumn;
                case SpecialEffectName.DestroyNeighbors:
                    return _destroyNeighbors;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}