﻿using System.Collections.Generic;
using Core.Board.Model;
using Core.Board.View;
using Core.Domain;

namespace Core.Board.Presenter.SpecialEffect
{
    public interface ISpecialEffect
    {
        SpecialEffectName Name { get; }

        void ApplyEffect(AbstractBoardModel board, AbstractBoardView view, Cell specialCell);
        
        IEnumerable<Cell> GetAffectedCells(AbstractBoardModel board, BoardPoint boardPoint);
    }
}