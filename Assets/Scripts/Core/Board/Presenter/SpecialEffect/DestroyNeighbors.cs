﻿using System.Collections.Generic;
using System.Linq;
using Core.Board.Model;
using Core.Board.View;
using Core.Domain;

namespace Core.Board.Presenter.SpecialEffect
{
    public class DestroyNeighbors : ISpecialEffect
    {
        public SpecialEffectName Name => SpecialEffectName.DestroyNeighbors;

        public IEnumerable<Cell> GetAffectedCells(AbstractBoardModel board, BoardPoint boardPoint)
        {
            return board.GetNeighborCells(boardPoint);
        }

        public void ApplyEffect(AbstractBoardModel board, AbstractBoardView view, Cell specialCell)
        {
            var neighborCells = GetAffectedCells(board, specialCell.BoardPoint).ToList();
            foreach (Cell cell in neighborCells)
            {
                if (cell.IsSpecial() && !cell.Equals(specialCell))
                {
                    continue;
                }
                board.RemoveCell(cell.BoardPoint);
                view.RemoveCell(cell.BoardPoint);
                view.EmitDestroyParticles(cell.BoardPoint, cell.Color.GetColor());
            }
            view.EmitDestroySpecialParticles(specialCell.BoardPoint, specialCell.Color.GetColor());
        }
    }
}