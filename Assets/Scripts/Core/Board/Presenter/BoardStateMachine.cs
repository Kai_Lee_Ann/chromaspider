﻿using System;
using Core.Board.Presenter.State;
using Core.Board.View;
using Core.Service.Event;
using UnityEngine;

namespace Core.Board.Presenter
{
    public class BoardStateMachine : MonoBehaviour
    {
        public AbstractBoardView view;
        public GameStateFactory gameStateFactory;
        public EventMessenger eventMessenger;

        private IGameState _currentState;
        private IGameState _resumeGameState;

        private void Awake()
        {
            eventMessenger.AddListener(GameEvent.GameStarted, new Action(Pause));
            eventMessenger.AddListener(GameEvent.GameWon, new Action(Pause));
            eventMessenger.AddListener(GameEvent.GameWonNewRecord, new Action(Pause));
            eventMessenger.AddListener(GameEvent.GameLost, new Action(Pause));
            view.AddOnMouseDownCallback(point => _currentState.OnBoardPointSelected(point));
            view.AddOnMouseDragCallback(point => _currentState.OnBoardPointDrag(point));
        }

        private void Start()
        {
            _currentState = gameStateFactory.GetState(GameStateName.GameStart);
            _currentState.OnStateEnter();
            Pause();
        }

        private void Update()
        {
            _currentState.Update();
            var nextStateName = _currentState.NextStateName;
            if (nextStateName == null)
            {
                return;
            }
            // Debug.Log($"Changing state to: {nextStateName.Value}");
            _currentState = gameStateFactory.GetState(nextStateName.Value);
            _currentState.OnStateEnter();
        }

        private void OnDrawGizmosSelected()
        {
            _currentState?.OnDrawGizmosSelected();
        }

        public void Pause()
        {
            if (_currentState.StateName == GameStateName.Pause)
            {
                return;
            }
            _resumeGameState = _currentState;
            _currentState = gameStateFactory.GetState(GameStateName.Pause);
            eventMessenger.SendMessage(GameEvent.GamePaused);
        }

        public void Unpause()
        {
            Debug.Assert(_resumeGameState?.StateName != GameStateName.Pause);
            _currentState = _resumeGameState;
            _resumeGameState = null;
            eventMessenger.SendMessage(GameEvent.GameUnpaused);
        }
    }
}