﻿using System.Collections.Generic;
using System.Linq;
using Core.Board.Model;
using Core.Board.View;
using Core.Domain;
using Core.Service.Event;
using UnityEngine;

namespace Core.Board.Presenter.State
{
    public class MoveCellsDown : TimedGameState
    {
        private const float MoveDownTime = 0.4f;

        private readonly AbstractBoardModel _board;
        private readonly AbstractBoardView _view;
        private readonly EventMessenger _eventMessenger;
        private IReadOnlyCollection<CellMovement> _cellMovements;

        public MoveCellsDown(AbstractBoardModel board, AbstractBoardView view, EventMessenger eventMessenger)
        {
            _board = board;
            _view = view;
            _eventMessenger = eventMessenger;
            StateName = GameStateName.MoveCellsDown;
        }

        public override void OnStateEnter()
        {
            base.OnStateEnter();
            _cellMovements = _board.FindCellsDownMovement();
            if (_cellMovements.Any())
            {
                _eventMessenger.SendMessage(GameEvent.MoveDownStart);
            }
        }

        public override void Update()
        {
            if (!_cellMovements.Any())
            {
                NextStateName = GameStateName.CreateCells;
                return;
            }

            foreach (CellMovement cellMovement in _cellMovements)
            {
                _view.MoveCellDown(cellMovement.StartPoint, cellMovement.TargetPoint, elapsedTime / MoveDownTime);
            }

            if (elapsedTime < MoveDownTime)
            {
                elapsedTime += Time.deltaTime;
                return;
            }

            NextStateName = GameStateName.CreateCells;
        }
    }
}