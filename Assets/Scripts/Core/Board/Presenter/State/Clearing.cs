﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Core.Board.Model;
using Core.Board.Presenter.SpecialEffect;
using Core.Board.View;
using Core.Domain;
using Core.Service.Event;
using Core.Service.ScoreService;
using UnityEngine;
using Util;

namespace Core.Board.Presenter.State
{
    public class Clearing : TimedGameState
    {
        private const float FirstClearingTime = 0.2f;
        private const float ClearingTime = 0.3f;
        private const float TimeBetweenClearings = 0.3f;

        private readonly AbstractBoardModel _board;
        private readonly AbstractBoardView _view;
        private readonly AbstractScoreService _scoreService;
        private readonly SpecialEffectFactory _specialEffectFactory;
        private readonly EventMessenger _eventMessenger;
        private readonly CoroutineExecutor _coroutineExecutor;
        private readonly List<CellMovement> _cellsMovementToUnite;
        private readonly List<Cell> _cellsToCreate;
        private readonly List<Cell> _cellsToRemove;
        private float _clearingTime;
        private CoroutineExecutor.Task _clearingTask;

        public Clearing(
            AbstractBoardModel board,
            AbstractBoardView view,
            AbstractScoreService scoreService,
            CoroutineExecutor coroutineExecutor,
            SpecialEffectFactory specialEffectFactory,
            EventMessenger eventMessenger
        )
        {
            _board = board;
            _view = view;
            _scoreService = scoreService;
            _specialEffectFactory = specialEffectFactory;
            _eventMessenger = eventMessenger;
            _coroutineExecutor = coroutineExecutor;
            _cellsMovementToUnite = new List<CellMovement>();
            _cellsToCreate = new List<Cell>();
            _cellsToRemove = new List<Cell>();
            StateName = GameStateName.Clearing;
        }

        public override void OnStateEnter()
        {
            base.OnStateEnter();
            _cellsMovementToUnite.Clear();
            _cellsToCreate.Clear();
            _cellsToRemove.Clear();

            var matches = _board.FindMatchSequence();
            if (!matches.Any())
            {
                return;
            }
            ClearMatchSequence(matches);
            if (!_scoreService.IsGameStarting)
            {
                _eventMessenger.SendMessage(GameEvent.AllMatchesCleared);
            }
            _clearingTime = _scoreService.IsGameStarting ? FirstClearingTime : ClearingTime;
        }

        private void ClearMatchSequence(IList<IList<Match>> matchSequence)
        {
            var clearMatchesTaskBuilder = CoroutineExecutor.Task.Builder;
            for (var order = 0; order < matchSequence.ToList().Count; order++)
            {
                var matches = matchSequence[order];
                clearMatchesTaskBuilder.Add(ClearMatches(matches), order * TimeBetweenClearings);
            }
            _clearingTask = clearMatchesTaskBuilder.Build();
            _coroutineExecutor.Execute(_clearingTask);
        }

        private IEnumerator ClearMatches(IList<Match> matches)
        {
            _eventMessenger.SendMessage(GameEvent.MatchClearStart, matches);
            foreach (var match in matches)
            {
                yield return ClearMatch(match);
            }
        }

        private IEnumerator ClearMatch(Match match)
        {
            if (match.SpecialCell != null)
            {
                ActivateSpecialCell(match.SpecialCell.Value);
                yield break;
            }

            if (match.CreatedCell == null)
            {
                foreach (var cell in match.Cells)
                {
                    if (cell.IsSpecial())
                    {
                        continue;
                    }
                    RemoveCell(cell);
                    _view.EmitDestroyParticles(cell.BoardPoint, cell.Color.GetColor());
                }
                yield break;
            }

            var cellToCreate = match.CreatedCell.Value;
            foreach (var cell in match.Cells)
            {
                _cellsMovementToUnite.Add(
                    new CellMovement(
                        cell.BoardPoint,
                        cellToCreate.BoardPoint
                    )
                );
                _cellsToRemove.Add(cell);
            }

            if (_scoreService.IsGameStarting)
            {
                yield break;
            }
            _cellsToCreate.Add(cellToCreate);
            _eventMessenger.SendMessage(GameEvent.SpecialCellCreated);
        }

        private void ActivateSpecialCell(Cell cell)
        {
            var specialEffect = _specialEffectFactory.GetSpecialEffect(cell.SpecialEffectName);
            specialEffect.ApplyEffect(_board, _view, cell);
            _eventMessenger.SendMessage(GameEvent.SpecialCellActivated, cell.SpecialEffectName);
            // Debug.Log($"Special cell activated: {cell.ToString()}");
        }
        
        public override void Update()
        {
            if (!_clearingTask.Finished)
            {
                return;
            }
            
            foreach (var cellMovement in _cellsMovementToUnite)
            {
                _view.MoveCell(cellMovement.StartPoint, cellMovement.TargetPoint, elapsedTime / _clearingTime);
            }

            if (elapsedTime < _clearingTime)
            {
                elapsedTime += Time.deltaTime;
                return;
            }

            foreach (var cell in _cellsToRemove)
            {
                RemoveCell(cell);
            }

            foreach (var cell in _cellsToCreate)
            {
                CreateCell(cell);
            }

            NextStateName = GameStateName.MoveCellsDown;
        }

        private void CreateCell(Cell createdCell)
        {
            _board.CreateCell(createdCell);
            _view.CreateCell(createdCell);
            // Debug.Log($"Cell created: {createdCell.ToString()}");
        }

        private void RemoveCell(Cell? cell)
        {
            if (cell == null)
            {
                return;
            }

            _board.RemoveCell(cell.Value.BoardPoint);
            _view.RemoveCell(cell.Value.BoardPoint);
        }
    }
}