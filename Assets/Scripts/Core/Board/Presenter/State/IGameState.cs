﻿using Core.Domain;

namespace Core.Board.Presenter.State
{
    public interface IGameState
    {
        GameStateName? NextStateName { get; }

        GameStateName StateName { get; }

        void Update();

        void OnStateEnter();

        void OnBoardPointSelected(BoardPoint boardPoint);

        void OnBoardPointDrag(BoardPoint boardPoint);
        
        void OnDrawGizmosSelected();
    }
}