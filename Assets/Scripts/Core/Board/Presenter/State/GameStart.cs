﻿using Core.Service.Event;

namespace Core.Board.Presenter.State
{
    public class GameStart : TimedGameState
    {
        private readonly IGameState _createCellsState;
        private readonly EventMessenger _eventMessenger;

        public GameStart(IGameState createCellsState, EventMessenger eventMessenger)
        {
            _createCellsState = createCellsState;
            _eventMessenger = eventMessenger;
            StateName = GameStateName.GameStart;
        }

        public override void OnStateEnter()
        {
            base.OnStateEnter();
            _eventMessenger.SendMessage(GameEvent.GameStarted);
            _createCellsState.OnStateEnter();
        }

        public override void Update()
        {
            _createCellsState.Update();
            NextStateName = _createCellsState.NextStateName;
        }
    }
}