﻿using System.Linq;
using Core.Board.Model;
using Core.Board.View;
using Core.Domain;
using Core.Service.Event;
using UnityEngine;

namespace Core.Board.Presenter.State
{
    public class Swap : TimedGameState
    {
        private const float SwapTime = 0.25f;

        private readonly AbstractBoardModel _board;
        private readonly AbstractBoardView _view;
        private readonly EventMessenger _eventMessenger;

        private BoardPoint _firstCell;
        private BoardPoint _secondCell;

        public Swap(AbstractBoardModel board, AbstractBoardView view, EventMessenger eventMessenger)
        {
            _board = board;
            _view = view;
            _eventMessenger = eventMessenger;
            StateName = GameStateName.Swap;
        }

        public override void OnStateEnter()
        {
            base.OnStateEnter();
            _board.SwapSelectedCells();

            Debug.Assert(_board.FirstSwapBoardPoint != null, "_board.FirstSwapCell != null");
            Debug.Assert(_board.SecondSwapBoardPoint != null, "_board.SecondSwapCell != null");

            _firstCell = _board.FirstSwapBoardPoint.Value;
            _secondCell = _board.SecondSwapBoardPoint.Value;
            _eventMessenger.SendMessage(GameEvent.SwapMade);
        }

        public override void Update()
        {
            _view.SwapCells(_firstCell, _secondCell, elapsedTime / SwapTime);
            if (elapsedTime < SwapTime)
            {
                elapsedTime += Time.deltaTime;
                return;
            }

            if (_board.FindMatchSequence().Any())
            {
                _eventMessenger.SendMessage(GameEvent.TurnMade);
                NextStateName = GameStateName.Clearing;
            }
            else
            {
                _board.FirstSwapBoardPoint = _secondCell;
                _board.SecondSwapBoardPoint = _firstCell;
                NextStateName = GameStateName.SwapBack;
            }
        }
    }
}