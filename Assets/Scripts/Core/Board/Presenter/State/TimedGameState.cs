﻿using Core.Domain;

namespace Core.Board.Presenter.State
{
    public abstract class TimedGameState : IGameState
    {
        public GameStateName? NextStateName { get; protected set; }
        
        public GameStateName StateName { get; protected set; }

        protected float elapsedTime;
        
        public virtual void OnStateEnter()
        {
            elapsedTime = 0;
            NextStateName = null;
        }

        public virtual void Update()
        {
        }

        public virtual void OnBoardPointSelected(BoardPoint boardPoint)
        {
        }

        public virtual void OnBoardPointDrag(BoardPoint boardPoint)
        {
        }

        public virtual void OnDrawGizmosSelected()
        {
        }
    }
}