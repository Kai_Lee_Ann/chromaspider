﻿using Core.Board.Model;
using Core.Board.View;
using Core.Domain;
using Core.Service.Event;
using UnityEngine;

namespace Core.Board.Presenter.State
{
    public class BoardReset : TimedGameState
    {
        private const float ClearingTime = 0.5f;

        private readonly AbstractBoardModel _board;
        private readonly AbstractBoardView _view;
        private readonly CellPool _cellPool;
        private readonly EventMessenger _eventMessenger;

        public BoardReset(AbstractBoardModel board, AbstractBoardView view, EventMessenger eventMessenger)
        {
            _board = board;
            _view = view;
            _eventMessenger = eventMessenger;
            StateName = GameStateName.BoardReset;
        }

        public override void OnStateEnter()
        {
            base.OnStateEnter();
            _eventMessenger.SendMessage(GameEvent.BoardReset);
            var removedCells = _board.RemoveAllCells();
            foreach (var cell in removedCells)
            {
                _view.RemoveCell(cell.BoardPoint);
                _view.EmitDestroyParticles(cell.BoardPoint, cell.Color.GetColor());
            }
        }

        public override void Update()
        {
            if (elapsedTime < ClearingTime)
            {
                elapsedTime += Time.deltaTime;
                return;
            }
            NextStateName = GameStateName.MoveCellsDown;
        }
    }
}