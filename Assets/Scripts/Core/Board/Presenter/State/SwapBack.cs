﻿using Core.Board.Model;
using UnityEngine;

namespace Core.Board.Presenter.State
{
    public class SwapBack : TimedGameState
    {
        private readonly IGameState _swapState;

        private readonly AbstractBoardModel _board;
        private readonly GameObject[,] _cells;
        private GameObject _firstCell;
        private GameObject _secondCell;
        private Vector3 _firstCellTargetWorldPosition;
        private Vector3 _secondCellTargetWorldPosition;

        public SwapBack(IGameState swapState)
        {
            _swapState = swapState;
            StateName = GameStateName.SwapBack;
        }

        public override void OnStateEnter()
        {
            base.OnStateEnter();
            _swapState.OnStateEnter();
        }

        public override void Update()
        {
            _swapState.Update();
            if (_swapState.NextStateName == null)
            {
                return;
            }
            NextStateName = GameStateName.WaitForTurn;
        }
    }
}