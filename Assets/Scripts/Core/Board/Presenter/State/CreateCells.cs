﻿using System.Linq;
using Core.Board.Model;
using Core.Board.View;
using UnityEngine;

namespace Core.Board.Presenter.State
{
    public class CreateCells : TimedGameState
    {
        private const float CreateCellsTime = 0.25f;
        private readonly AbstractBoardModel _board;
        private readonly AbstractBoardView _view;

        public CreateCells(AbstractBoardModel board, AbstractBoardView view)
        {
            _board = board;
            _view = view;
            StateName = GameStateName.CreateCells;
        }

        public override void OnStateEnter()
        {
            base.OnStateEnter();
            var newCellPositions = _board.FillEmptyCells();
            foreach (var cellPosition in newCellPositions)
            {
                _view.CreateCell(cellPosition);
            }
        }
        
        public override void Update()
        {
            if (elapsedTime < CreateCellsTime)
            {
                elapsedTime += Time.deltaTime;
                return;
            }

            if (_board.FindMatchSequence().Any())
            {
                NextStateName = GameStateName.Clearing;
                return;
            }

            if (_board.FindSolution() == null)
            {
                NextStateName = GameStateName.BoardReset;
                return;
            }

            NextStateName = GameStateName.WaitForTurn;
        }
        
    }
}