﻿using System;
using Core.Board.Model;
using Core.Board.View;
using Core.Domain;
using Core.Service.Event;
using Persistence;
using UnityEngine;

namespace Core.Board.Presenter.State
{
    public class WaitForTurn : TimedGameState
    {
        private const float TimeBeforeHintShown = 7.0f;

        private readonly AbstractBoardModel _board;
        private readonly AbstractBoardView _view;
        private readonly PlayerSettings _playerSettings;
        private readonly EventMessenger _eventMessenger;

        private bool _hintShown;
        private BoardPoint? _firstHintCell;
        private BoardPoint? _secondHintCell;
        private BoardPoint? _firstCell;

        public WaitForTurn(
            AbstractBoardModel board,
            AbstractBoardView view,
            PlayerSettings playerSettings,
            EventMessenger eventMessenger
        )
        {
            _board = board;
            _view = view;
            _playerSettings = playerSettings;
            _eventMessenger = eventMessenger;
            StateName = GameStateName.WaitForTurn;
        }

        public override void OnStateEnter()
        {
            base.OnStateEnter();
            _eventMessenger.SendMessage(GameEvent.NewTurnStart);
            _firstCell = null;
            _firstHintCell = null;
            _secondHintCell = null;
            _hintShown = false;
        }

        public override void OnBoardPointSelected(BoardPoint boardPoint)
        {
            if (_firstCell == null)
            {
                _board.FirstSwapBoardPoint = boardPoint;
                _firstCell = boardPoint;
                if (IsHintCell(boardPoint))
                {
                    HideHint();
                }

                _view.SelectCell(boardPoint);
                return;
            }

            if (_firstCell == boardPoint)
            {
                _view.Unselect(_firstCell.Value);
                _firstCell = null;
                return;
            }

            _board.SecondSwapBoardPoint = boardPoint;
            if (!_board.IsSelectedCellsSwappable())
            {
                _view.Unselect(_firstCell.Value);
                _board.FirstSwapBoardPoint = boardPoint;
                _board.SecondSwapBoardPoint = null;
                _firstCell = boardPoint;
                _view.SelectCell(_firstCell.Value);
                return;
            }

            HideHint();
            NextStateName = GameStateName.Swap;
        }

        public override void OnBoardPointDrag(BoardPoint boardPoint)
        {
            if (_firstCell == null)
            {
                return;
            }

            if (_board.FirstSwapBoardPoint == boardPoint)
            {
                return;
            }

            _board.SecondSwapBoardPoint = boardPoint;
            if (!_board.IsSelectedCellsSwappable())
            {
                _board.SecondSwapBoardPoint = null;
                return;
            }

            HideHint();
            NextStateName = GameStateName.Swap;
        }

        private bool IsHintCell(BoardPoint boardPoint)
        {
            return _hintShown && (boardPoint == _firstHintCell || boardPoint == _secondHintCell);
        }

        public override void Update()
        {
            if (!_playerSettings.AutoHintsEnabled)
            {
                return;
            }

            if (_hintShown)
            {
                return;
            }

            if (elapsedTime < TimeBeforeHintShown)
            {
                elapsedTime += Time.deltaTime;
                return;
            }

            ShowHint();
            elapsedTime = 0;
        }

        private void ShowHint()
        {
            var solution = _board.FindSolution();

            _firstHintCell = solution.FirstCell;
            _secondHintCell = solution.SecondCell;
            _view.SelectHintCells(_firstHintCell.Value, _secondHintCell.Value);

            _hintShown = true;
        }

        private void HideHint()
        {
            if (!_hintShown)
            {
                return;
            }

            Debug.Assert(_firstHintCell != null, nameof(_firstHintCell) + " != null");
            Debug.Assert(_secondHintCell != null, nameof(_secondHintCell) + " != null");
            _view.HideHint(_firstHintCell.Value, _secondHintCell.Value);
            _hintShown = false;
        }

        public override void OnDrawGizmosSelected()
        {
            if (_firstCell != null)
            {
                _view.DrawGizmoCube(_firstCell.Value, Color.red);
                foreach (var cell in _board.GetNeighborCells(_firstCell.Value))
                {
                    _view.DrawGizmoCube(cell.BoardPoint, Color.red);
                }
            }

            if (_firstHintCell != null)
            {
                _view.DrawGizmoCube(_firstHintCell.Value, Color.yellow);
            }

            if (_secondHintCell != null)
            {
                _view.DrawGizmoCube(_secondHintCell.Value, Color.yellow);
            }

            var specialCells = _board.FindSpecialCells();
            foreach (var specialCell in specialCells)
            {
                switch (specialCell.SpecialEffectName)
                {
                    case SpecialEffectName.None:
                        break;
                    case SpecialEffectName.DestroyRow:
                        _view.DrawGizmoCube(specialCell.BoardPoint, Color.magenta);
                        break;
                    case SpecialEffectName.DestroyColumn:
                        _view.DrawGizmoCube(specialCell.BoardPoint, Color.cyan);
                        break;
                    case SpecialEffectName.DestroyNeighbors:
                        _view.DrawGizmoCube(specialCell.BoardPoint, Color.blue);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }
    }
}