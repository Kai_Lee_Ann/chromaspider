﻿using System;
using Core.Board.Model;
using Core.Board.Presenter.SpecialEffect;
using Core.Board.View;
using Core.Service.Event;
using Core.Service.ScoreService;
using Persistence;
using UnityEngine;
using Util;

namespace Core.Board.Presenter.State
{
    public class GameStateFactory : MonoBehaviour
    {
        public AbstractBoardView view;
        public AbstractBoardModel board;
        public AbstractScoreService scoreService;
        public PlayerSettings playerSettings;
        public SpecialEffectFactory specialEffectFactory;
        public CoroutineExecutor coroutineExecutor;
        public EventMessenger eventMessenger;

        private CreateCells _createCells;
        private GameStart _gameStart;
        private WaitForTurn _waitForTurn;
        private Swap _swap;
        private SwapBack _swapBack;
        private MoveCellsDown _moveCellsDown;
        private BoardReset _boardReset;
        private Pause _pause;
        private Clearing _clearing;

        private void Awake()
        {
            _createCells = new CreateCells(board, view);
            _gameStart = new GameStart(_createCells, eventMessenger);
            _waitForTurn = new WaitForTurn(board, view, playerSettings, eventMessenger);
            _swap = new Swap(board, view, eventMessenger);
            _swapBack = new SwapBack(_swap);
            _moveCellsDown = new MoveCellsDown(board, view, eventMessenger);
            _boardReset = new BoardReset(board, view, eventMessenger);
            _pause = new Pause();
            _clearing = new Clearing(
                board,
                view,
                scoreService,
                coroutineExecutor,
                specialEffectFactory,
                eventMessenger
            );
        }

        public IGameState GetState(GameStateName gameStateName)
        {
            switch (gameStateName)
            {
                case GameStateName.GameStart:
                    return _gameStart;
                case GameStateName.WaitForTurn:
                    return _waitForTurn;
                case GameStateName.Swap:
                    return _swap;
                case GameStateName.SwapBack:
                    return _swapBack;
                case GameStateName.Clearing:
                    return _clearing;
                case GameStateName.MoveCellsDown:
                    return _moveCellsDown;
                case GameStateName.CreateCells:
                    return _createCells;
                case GameStateName.BoardReset:
                    return _boardReset;
                case GameStateName.Pause:
                    return _pause;
                default:
                    throw new ArgumentOutOfRangeException(nameof(gameStateName), gameStateName, null);
            }
        }
    }
}