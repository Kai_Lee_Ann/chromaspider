﻿namespace Core.Board.Presenter.State
{
    public enum GameStateName
    {
        GameStart,
        WaitForTurn,
        Swap,
        SwapBack,
        Clearing,
        MoveCellsDown,
        CreateCells,
        BoardReset,
        Pause
    }
}