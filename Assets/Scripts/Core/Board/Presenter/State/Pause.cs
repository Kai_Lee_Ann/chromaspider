﻿using Core.Domain;

namespace Core.Board.Presenter.State
{
    public class Pause : IGameState
    {
        public GameStateName? NextStateName => null;

        public GameStateName StateName => GameStateName.Pause;

        public void OnStateEnter()
        {
        }

        public void OnBoardPointSelected(BoardPoint boardPoint)
        {
        }

        public void OnBoardPointDrag(BoardPoint boardPoint)
        {
        }

        public void OnDrawGizmosSelected()
        {
        }

        public void Update()
        {
        }
    }
}