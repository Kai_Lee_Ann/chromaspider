﻿using System;
using System.Collections;
using System.Collections.Generic;
using Core.Domain;
using Core.Settings;
using UnityEngine;

namespace Core.Board.View
{
    public class CellPool : MonoBehaviour
    {
        [SerializeField] private GameSettings gameSettings;
        [SerializeField] private Sprite[] cellSprites;
        [SerializeField] private GameObject cellPrefab;

        private GameObject[] _gameObjects;
        private SpriteRenderer[] _spriteRenderers;
        private Queue<int> _freeCells;

        private void Awake()
        {
            Debug.Assert(cellSprites.Length >= gameSettings.colorCount, $"Sprites count should be {gameSettings.colorCount} at least");

            int rows = gameSettings.rowSize;
            int columns = gameSettings.columnSize;

            int capacity = 2 * rows * columns;
            _gameObjects = new GameObject[capacity];
            _spriteRenderers = new SpriteRenderer[capacity];
            _freeCells = new Queue<int>(capacity);

            for (int i = 0; i < capacity; i++)
            {
                CacheGameObject(Instantiate(cellPrefab, gameObject.transform), i);
            }
        }

        private void CacheGameObject(GameObject cell, int i)
        {
            cell.SetActive(false);
            _spriteRenderers[i] = cell.GetComponent<SpriteRenderer>();
            _gameObjects[i] = cell;
            _freeCells.Enqueue(i);
        }

        public GameObject GetCell(CellColor colorIndex)
        {
            var sprite = cellSprites[(int) colorIndex];
            int cellIndex = _freeCells.Dequeue();
            _spriteRenderers[cellIndex].sprite = sprite;
            var cell = _gameObjects[cellIndex];
            cell.SetActive(true);

            return cell;
        }

        public void ReleaseCell(GameObject cell, float releaseTime, Action<GameObject> onDisable)
        {
            _freeCells.Enqueue(Array.FindIndex(_gameObjects, o => ReferenceEquals(o, cell)));
            StartCoroutine(DisableCell(cell, releaseTime, onDisable));
        }

        private static IEnumerator DisableCell(GameObject cell, float releaseTime, Action<GameObject> onDisable)
        {
            yield return new WaitForSeconds(releaseTime);
            onDisable.Invoke(cell);
            cell.SetActive(false);
        }
    }
}