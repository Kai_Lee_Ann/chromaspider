﻿using System;
using Core.Domain;
using Core.Settings;
using UnityEngine;

namespace Core.Board.View
{
    [RequireComponent(typeof(BoxCollider2D))]
    public class BoardView : AbstractBoardView
    {
        private const float ReleaseTime = 0.5f;

        public GameSettings gameSettings;
        public CellPool cellPool;
        public AnimationCurve fallDownCurve = AnimationCurve.Linear(0.0f, 0.0f, 1.0f, 1.0f);
        public Sprite destroyRowSpecialEffectSprite;
        public Sprite destroyColumnSpecialEffectSprite;
        public Sprite destroyNeighborsSpecialEffectSprite;
        public ParticleSystem destroyCellParticleSystem;
        public ParticleSystem destroySpecialCellParticleSystem;

        private NullableGameObject[,] _cells;
        private BoxCollider2D _boardCollider;
        private int _columnSize;
        private int _rowSize;
        private float _boardYOffset;
        private float _boardXOffset;
        private Camera _camera;

        private Action<BoardPoint> _onMouseDownCallback;
        private Action<BoardPoint> _onMouseDragCallback;

        private void Awake()
        {
            _columnSize = gameSettings.columnSize;
            _rowSize = gameSettings.rowSize;
            _cells = new NullableGameObject[_columnSize, _rowSize];

            _boardXOffset = -_columnSize / 2f;
            _boardYOffset = -_rowSize / 2f;

            _boardCollider = GetComponent<BoxCollider2D>();
            _boardCollider.size = new Vector2(_columnSize, _rowSize);

            Debug.Assert(Camera.main != null, "Camera.main != null");
            _camera = Camera.main;
        }

        private void OnMouseDown()
        {
            var worldPoint = _camera.ScreenToWorldPoint(Input.mousePosition);
            var boardPoint = WorldPointToBoardPoint(worldPoint);
            // Debug.Log($"WorldPointToBoardPoint: (worldPoint: {worldPoint.ToString()}) => boardPoint:{boardPoint.ToString()}");
            if (boardPoint == null)
            {
                return;
            }

            _onMouseDownCallback.Invoke(boardPoint.Value);
        }

        private void OnMouseDrag()
        {
            var worldPoint = _camera.ScreenToWorldPoint(Input.mousePosition);
            var boardPoint = WorldPointToBoardPoint(worldPoint);
            // Debug.Log($"WorldPointToBoardPoint: (worldPoint: {worldPoint.ToString()}) => boardPoint:{boardPoint.ToString()}");
            if (boardPoint == null)
            {
                return;
            }

            _onMouseDragCallback.Invoke(boardPoint.Value);
        }

        public override void CreateCell(Cell cell)
        {
            var boardPoint = cell.BoardPoint;
            var cellGameObject = cellPool.GetCell(cell.Color);
            var nullableGameObject = new NullableGameObject(cellGameObject);
            nullableGameObject.GameObject.transform.position = BoardPointToWorldPoint(boardPoint);
            this[boardPoint] = nullableGameObject;

            if (cell.IsSpecial())
            {
                AddSpecialEffectSprite(cellGameObject, cell);
            }
            else
            {
                GetSpecialEffect(cellGameObject).SetActive(false);
            }

            PlayAnimation(nullableGameObject, CellAnimations.GrowAnimation);
        }

        private void AddSpecialEffectSprite(GameObject cellGameObject, Cell cell)
        {
            if (cell.SpecialEffectName == SpecialEffectName.None)
            {
                return;
            }

            var specialEffect = GetSpecialEffect(cellGameObject);
            specialEffect.SetActive(true);

            var spriteRenderer = specialEffect.GetComponent<SpriteRenderer>();
            var color = cell.Color.GetColor();
            spriteRenderer.color = new Color(color.r, color.g, color.b, spriteRenderer.color.a);
            switch (cell.SpecialEffectName)
            {
                case SpecialEffectName.DestroyRow:
                    spriteRenderer.sprite = destroyRowSpecialEffectSprite;
                    break;
                case SpecialEffectName.DestroyColumn:
                    spriteRenderer.sprite = destroyColumnSpecialEffectSprite;
                    break;
                case SpecialEffectName.DestroyNeighbors:
                    spriteRenderer.sprite = destroyNeighborsSpecialEffectSprite;
                    break;
                case SpecialEffectName.None:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            PlayAnimation(spriteRenderer.gameObject, CellAnimations.ZoomAnimation);
        }

        public override void SelectCell(BoardPoint point)
        {
            PlayAnimation(this[point], CellAnimations.SelectAnimation);
        }

        public override void SelectHintCells(params BoardPoint[] boardPoints)
        {
            foreach (var boardPoint in boardPoints)
            {
                PlayAnimation(this[boardPoint], CellAnimations.HintAnimation);
            }
        }

        public override void RemoveCell(BoardPoint boardPoint)
        {
            var nullableGameObject = this[boardPoint];
            if (nullableGameObject == null)
            {
                return;
            }

            var specialEffect = GetSpecialEffect(nullableGameObject.GameObject);
            if (specialEffect.activeSelf)
            {
                PlayAnimation(specialEffect, CellAnimations.ZoomOutAnimation);
            }

            cellPool.ReleaseCell(nullableGameObject.GameObject, ReleaseTime, cellObject =>
            {
                StopAnimation(cellObject);
                StopAnimation(GetSpecialEffect(cellObject));
            });
            this[boardPoint] = null;
            PlayAnimation(nullableGameObject, CellAnimations.ShatterAnimation);
        }

        public override void MoveCellDown(BoardPoint startPosition, BoardPoint targetPosition, float time)
        {
            var nullableGameObject = this[startPosition];
            if (nullableGameObject == null)
            {
                return;
            }

            nullableGameObject.GameObject.transform.position = Vector3.Lerp(
                BoardPointToWorldPoint(startPosition),
                BoardPointToWorldPoint(targetPosition),
                fallDownCurve.Evaluate(time)
            );
            if (time < 1)
            {
                return;
            }

            this[targetPosition] = this[startPosition];
            this[startPosition] = null;
        }

        public override void MoveCell(BoardPoint cellPoint, BoardPoint targetPoint, float time)
        {
            var nullableGameObject = this[cellPoint];
            if (nullableGameObject == null)
            {
                return;
            }

            nullableGameObject.GameObject.transform.position = Vector3.Lerp(
                BoardPointToWorldPoint(cellPoint),
                BoardPointToWorldPoint(targetPoint),
                fallDownCurve.Evaluate(time)
            );
        }

        public override void EmitDestroyParticles(BoardPoint boardPoint, Color color)
        {
            var emitParams = new ParticleSystem.EmitParams
            {
                position = BoardPointToWorldPoint(boardPoint),
                applyShapeToPosition = true,
                startColor = color
            };
            destroyCellParticleSystem.Emit(emitParams, 5);
        }

        public override void EmitDestroySpecialParticles(BoardPoint boardPoint, Color color)
        {
            var emitParams = new ParticleSystem.EmitParams
            {
                position = BoardPointToWorldPoint(boardPoint),
                applyShapeToPosition = true,
                startColor = color
            };
            destroySpecialCellParticleSystem.Emit(emitParams, 40);
        }

        public override void AddOnMouseDownCallback(Action<BoardPoint> cellSelectedAction)
        {
            _onMouseDownCallback += cellSelectedAction;
        }

        public override void AddOnMouseDragCallback(Action<BoardPoint> cellDragAction)
        {
            _onMouseDragCallback += cellDragAction;
        }

        public override void SwapCells(BoardPoint startPosition, BoardPoint targetPosition, float time)
        {
            var firstNullableGameObject = this[startPosition];
            var secondNullableGameObject = this[targetPosition];
            Debug.Assert(firstNullableGameObject != null, "firstCell != null");
            Debug.Assert(secondNullableGameObject != null, "firstCell != null");

            if (time == 0)
            {
                PlayAnimation(firstNullableGameObject, CellAnimations.SwapAnimation);
                firstNullableGameObject.GameObject.GetComponent<SpriteRenderer>().sortingOrder = 1;
            }

            firstNullableGameObject.GameObject.transform.position = Vector3.Lerp(
                BoardPointToWorldPoint(startPosition),
                BoardPointToWorldPoint(targetPosition),
                time
            );
            secondNullableGameObject.GameObject.transform.position = Vector3.Lerp(
                BoardPointToWorldPoint(targetPosition),
                BoardPointToWorldPoint(startPosition),
                time
            );
            if (time < 1)
            {
                return;
            }

            var temp = this[targetPosition];
            this[targetPosition] = this[startPosition];
            this[startPosition] = temp;
            firstNullableGameObject.GameObject.GetComponent<SpriteRenderer>().sortingOrder = 0;
            StopAnimation(firstNullableGameObject);
        }

        private static void PlayAnimation(NullableGameObject nullableGameObject, string animationName)
        {
            if (nullableGameObject != null)
            {
                PlayAnimation(nullableGameObject.GameObject, animationName);
            }
        }

        private static void PlayAnimation(GameObject gameObject, string animationName)
        {
            var animator = gameObject.GetComponent<Animator>();
            animator.enabled = true;
            // Resets the animation. Explanation here: https://answers.unity.com/questions/623878/how-to-restart-mecanim-state-animation-or-play-it.html
            animator.Play(animationName, -1, 0);
        }

        private static void StopAnimation(NullableGameObject nullableGameObject)
        {
            if (nullableGameObject != null)
            {
                StopAnimation(nullableGameObject.GameObject);
            }
        }

        private static void StopAnimation(GameObject cell)
        {
            var animator = cell.GetComponent<Animator>();
            animator.WriteDefaultValues();
            animator.enabled = false;
        }

        private NullableGameObject this[BoardPoint point]
        {
            get => _cells[point.Column, point.Row];
            set => _cells[point.Column, point.Row] = value;
        }

        public override Vector2 BoardPointToWorldPoint(BoardPoint boardPoint)
        {
            return new Vector2(
                _boardXOffset + boardPoint.Column + 0.5f,
                _boardYOffset + boardPoint.Row + 0.5f
            );
        }

        private BoardPoint? WorldPointToBoardPoint(Vector2 worldPoint)
        {
            Vector2 point = _boardCollider.transform.InverseTransformPoint(worldPoint);
            var boardPoint = new BoardPoint(
                Convert.ToInt32(Mathf.Floor(point.x - _boardXOffset)),
                Convert.ToInt32(Mathf.Floor(point.y - _boardYOffset))
            );
            if (IsOutOfBounds(boardPoint))
            {
                return null;
            }

            return boardPoint;
        }

        private bool IsOutOfBounds(BoardPoint boardPoint)
        {
            return boardPoint.Row >= _rowSize ||
                   boardPoint.Column >= _columnSize ||
                   boardPoint.Row < 0 ||
                   boardPoint.Column < 0;
        }

        public override void Unselect(BoardPoint boardPoint)
        {
            StopAnimation(this[boardPoint]);
        }

        public override void HideHint(BoardPoint firstHintCell, BoardPoint secondHintCell)
        {
            StopAnimation(this[firstHintCell]);
            StopAnimation(this[secondHintCell]);
        }

        public override void DrawGizmoCube(BoardPoint boardPoint, Color color)
        {
            Gizmos.color = new Color(color.r, color.g, color.b, 0.5f);
            Gizmos.DrawCube(BoardPointToWorldPoint(boardPoint), Vector3.one);
        }

        private static GameObject GetSpecialEffect(GameObject o)
        {
            return o.transform.GetChild(0).transform.gameObject;
        }
        
        // Used to avoid gameObject expensive null checking
        private class NullableGameObject
        {
            public GameObject GameObject { get; }

            public NullableGameObject(GameObject gameObject)
            {
                GameObject = gameObject;
            }
        }

    }
    
}