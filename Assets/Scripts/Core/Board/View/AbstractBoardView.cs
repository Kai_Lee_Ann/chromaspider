﻿using System;
using Core.Domain;
using UnityEngine;

namespace Core.Board.View
{
    public abstract class AbstractBoardView : MonoBehaviour
    {
        public abstract void CreateCell(Cell cell);

        public abstract void  MoveCellDown(BoardPoint startPosition, BoardPoint endPosition, float time);

        public abstract void  SwapCells(BoardPoint startPosition, BoardPoint targetPosition, float time);

        public abstract void  SelectCell(BoardPoint point);

        public abstract void  SelectHintCells(params BoardPoint[] boardPoints);

        public abstract void  RemoveCell(BoardPoint boardPoint);

        public abstract Vector2 BoardPointToWorldPoint(BoardPoint boardPoint);

        public abstract void Unselect(BoardPoint boardPoint);
        
        public abstract void HideHint(BoardPoint firstHintCell, BoardPoint secondHintCell);
        
        public abstract void MoveCell(BoardPoint cellPoint, BoardPoint targetPoint, float time);

        public abstract void  EmitDestroyParticles(BoardPoint boardPoint, Color color);

        public abstract void  EmitDestroySpecialParticles(BoardPoint boardPoint, Color color);

        public abstract void  AddOnMouseDownCallback(Action<BoardPoint> cellSelectedAction);
        
        public abstract void  AddOnMouseDragCallback(Action<BoardPoint> cellDragAction);

        public abstract void DrawGizmoCube(BoardPoint boardPoint, Color color);
    }
}