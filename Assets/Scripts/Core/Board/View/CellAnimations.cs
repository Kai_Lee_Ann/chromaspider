﻿namespace Core.Board.View
{
    public static class CellAnimations
    {
        public const string HintAnimation = "Hint";
        public const string SelectAnimation = "Select";
        public const string SwapAnimation = "Swap";
        public const string ShatterAnimation = "Shatter";
        public const string GrowAnimation = "Grow";
        public const string ZoomAnimation = "Zoom";
        public const string ZoomOutAnimation = "ZoomOut";
    }
}