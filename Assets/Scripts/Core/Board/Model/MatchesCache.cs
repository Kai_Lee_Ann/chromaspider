﻿using System;
using System.Collections.Generic;
using Core.Domain;

namespace Core.Board.Model
{
    internal class MatchesCache
    {
        public IList<IList<Match>> Matches
        {
            get
            {
                if (!_dirty)
                {
                    return _matches;
                }

                _matches = _findMatchesFunction.Invoke();
                _dirty = false;
                return _matches;
            }
        }

        private readonly Func<IList<IList<Match>>> _findMatchesFunction;
        private IList<IList<Match>> _matches;
        private bool _dirty;

        public MatchesCache(Func<IList<IList<Match>>> findMatchesFunction)
        {
            _findMatchesFunction = findMatchesFunction;
        }

        public void Invalidate()
        {
            _dirty = true;
        }
    }
}