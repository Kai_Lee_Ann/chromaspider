﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Board.Presenter.SpecialEffect;
using Core.Domain;
using Core.Settings;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Core.Board.Model
{
    [RequireComponent(typeof(BoxCollider2D))]
    public class BoardModel : AbstractBoardModel
    {
        public GameSettings gameSettings;
        public SpecialEffectFactory specialEffectFactory;

        private CellState[,] _cells;
        private MatchesCache _matchCache;
        private int _columnSize;
        private int _rowSize;
        private int _minMatchLength;
        private MatchFactory _matchFactory;

        public override BoardPoint? FirstSwapBoardPoint { get; set; }

        public override BoardPoint? SecondSwapBoardPoint { get; set; }

        private void Awake()
        {
            _columnSize = gameSettings.columnSize;
            _rowSize = gameSettings.rowSize;
            _cells = new CellState[_columnSize, _rowSize];
            _minMatchLength = gameSettings.minMatchLength;
            _matchCache = new MatchesCache(FindOrderedMatches);
            _matchFactory = new MatchFactory(specialEffectFactory);
        }

        public override IEnumerable<Cell> FillEmptyCells()
        {
            _matchCache.Invalidate();
            foreach (var boardPoint in FindEmptyCells())
            {
                int color = Random.Range(0, gameSettings.colorCount);
                if (!Enum.IsDefined(typeof(CellColor), color))
                {
                    throw new ArgumentException($"Color {color} is not defined!");
                }

                var cellColor = (CellColor) color;
                this[boardPoint] = new CellState(cellColor);
                yield return new Cell(boardPoint, cellColor);
            }
        }

        public override void CreateCell(Cell cell)
        {
            _matchCache.Invalidate();
            this[cell.BoardPoint] = new CellState(cell);
        }

        public override IEnumerable<Cell> FindSpecialCells()
        {
            for (int column = 0; column < _columnSize; column++)
            {
                for (int row = 0; row < _rowSize; row++)
                {
                    var cell = _cells[column, row];
                    if (cell == null)
                    {
                        continue;
                    }

                    if (cell.IsSpecial())
                    {
                        yield return new Cell(column, row, cell);
                    }
                }
            }
        }

        public override IEnumerable<Cell> GetRow(int row)
        {
            for (int column = 0; column < _columnSize; ++column)
            {
                if (_cells[column, row] != null)
                {
                    yield return new Cell(column, row, _cells[column, row]);
                }
            }
        }

        public override IEnumerable<Cell> GetColumn(int column)
        {
            for (int row = 0; row < _rowSize; ++row)
            {
                if (_cells[column, row] != null)
                {
                    yield return new Cell(column, row, _cells[column, row]);
                }
            }
        }

        public override IEnumerable<Cell> GetNeighborCells(BoardPoint boardPoint)
        {
            int leftColumn = boardPoint.Column == 0 ? 0 : boardPoint.Column - 1;
            int rightColumn = boardPoint.Column == _columnSize - 1 ? _columnSize : boardPoint.Column + 2;
            int downRow = boardPoint.Row == 0 ? 0 : boardPoint.Row - 1;
            int topRow = boardPoint.Row == _rowSize - 1 ? _rowSize : boardPoint.Row + 2;

            for (int column = leftColumn; column < rightColumn; column++)
            {
                for (int row = downRow; row < topRow; row++)
                {
                    var point = new BoardPoint(column, row);
                    var cell = this[point];
                    if (cell != null)
                    {
                        yield return new Cell(point, cell);
                    }
                }
            }
        }

        public override IReadOnlyCollection<CellMovement> FindCellsDownMovement()
        {
            var moveDownPositions = new List<CellMovement>();
            for (int column = 0; column < _columnSize; column++)
            {
                int? lowestEmptyRow = null;
                for (int row = 0; row < _rowSize; row++)
                {
                    var cell = _cells[column, row];
                    if (cell == null)
                    {
                        if (lowestEmptyRow == null)
                        {
                            lowestEmptyRow = row;
                        }

                        continue;
                    }

                    if (lowestEmptyRow == null)
                    {
                        continue;
                    }

                    moveDownPositions.Add(
                        new CellMovement(
                            new BoardPoint(column, row),
                            new BoardPoint(column, lowestEmptyRow.Value)
                        )
                    );

                    _cells[column, row] = null;
                    _cells[column, lowestEmptyRow.Value] = cell;

                    lowestEmptyRow++;
                }
            }

            return moveDownPositions.AsReadOnly();
        }

        public override void SwapSelectedCells()
        {
            Debug.Assert(FirstSwapBoardPoint != null, nameof(FirstSwapBoardPoint) + " != null");
            Debug.Assert(SecondSwapBoardPoint != null, nameof(SecondSwapBoardPoint) + " != null");
            SwapCells(FirstSwapBoardPoint.Value, SecondSwapBoardPoint.Value);
        }

        private void SwapCells(BoardPoint cellA, BoardPoint cellB)
        {
            _matchCache.Invalidate();
            var temp = this[cellB];
            this[cellB] = this[cellA];
            this[cellA] = temp;
        }

        private IList<IList<Match>> FindOrderedMatches()
        {
            var matches = new List<IList<Match>>();
            
            var regularMatches = FindRegularMatches();
            if (!regularMatches.Any())
            {
                return matches;
            }
            var firstOrderMatches = new List<Match>();
            firstOrderMatches.AddRange(regularMatches);

            IList<Cell> specialCells = new List<Cell>();
            IList<Match> specialMatches = FindSpecialMatches(firstOrderMatches, specialCells).ToList();
            firstOrderMatches.AddRange(specialMatches);
            matches.Add(firstOrderMatches);

            while (specialMatches.Count != 0)
            {
                specialMatches = FindSpecialMatches(specialMatches, specialCells).ToList();
                matches.Add(specialMatches);
            }

            return matches;
        }

        private IEnumerable<Match> FindSpecialMatches(IEnumerable<Match> matches, ICollection<Cell> specialCells)
        {
            foreach (Match match in matches)
            {
                foreach (var cell in match.Cells)
                {
                    if (!cell.IsSpecial() || specialCells.Contains(cell))
                    {
                        continue;
                    }

                    specialCells.Add(cell);
                    ISpecialEffect specialEffect = specialEffectFactory.GetSpecialEffect(cell.SpecialEffectName);
                    yield return new Match(
                        specialEffect.GetAffectedCells(this, cell.BoardPoint).ToList(),
                        specialCell: cell
                    );
                }
            }
        }

        private List<Match> FindRegularMatches()
        {
            var matches = new List<Match>();
            var columnMatches = FindRowMatches();
            var rowMatches = FindColumnMatches();
            var crossMatches = UniteCrossMatches(columnMatches, rowMatches);

            matches.AddRange(columnMatches);
            matches.AddRange(rowMatches);
            matches.AddRange(crossMatches);
            return matches;
        }

        public override IList<IList<Match>> FindMatchSequence()
        {
            return _matchCache.Matches;
        }

        private IList<Match> FindRowMatches()
        {
            var matches = new List<Match>();
            for (int row = 0; row < _rowSize; row++)
            {
                CellColor? matchColor = null;
                var matchCells = new List<Cell>(_columnSize / 2);

                for (int column = 0; column < _columnSize; column++)
                {
                    var nextCell = _cells[column, row];
                    if (nextCell == null && matchColor == null)
                    {
                        continue;
                    }

                    if (nextCell != null && matchColor == nextCell.Color)
                    {
                        matchCells.Add(new Cell(column, row, nextCell));
                        continue;
                    }

                    if (matchCells.Count >= _minMatchLength)
                    {
                        matches.Add(CreateMatch(matchCells, MatchShape.Row));
                    }

                    matchColor = nextCell?.Color;
                    matchCells.Clear();

                    if (nextCell != null)
                    {
                        matchCells.Add(new Cell(column, row, nextCell));
                    }
                }

                if (matchCells.Count >= _minMatchLength)
                {
                    matches.Add(CreateMatch(matchCells, MatchShape.Row));
                }
            }

            return matches;
        }

        private IList<Match> FindColumnMatches()
        {
            var matches = new List<Match>();
            for (int column = 0; column < _columnSize; column++)
            {
                CellColor? matchColor = null;
                var matchCells = new List<Cell>(_rowSize / 2);

                for (int row = 0; row < _rowSize; row++)
                {
                    var nextCell = _cells[column, row];
                    if (nextCell == null && matchColor == null)
                    {
                        continue;
                    }

                    if (nextCell != null && matchColor == nextCell.Color)
                    {
                        matchCells.Add(new Cell(column, row, nextCell));
                        continue;
                    }

                    if (matchCells.Count >= _minMatchLength)
                    {
                        matches.Add(CreateMatch(matchCells, MatchShape.Column));
                    }

                    matchColor = nextCell?.Color;
                    matchCells.Clear();

                    if (nextCell != null)
                    {
                        matchCells.Add(new Cell(column, row, nextCell));
                    }
                }

                if (matchCells.Count >= _minMatchLength)
                {
                    matches.Add(CreateMatch(matchCells, MatchShape.Column));
                }
            }

            return matches;
        }

        private IEnumerable<Match> UniteCrossMatches(IList<Match> matchesA, IList<Match> matchesB)
        {
            var list = new List<Match>();
            var removeFromMatchesA = new SortedSet<int>();
            var removeFromMatchesB = new SortedSet<int>();
            for (var i = 0; i < matchesA.Count; i++)
            {
                var matchA = matchesA[i];
                for (var j = 0; j < matchesB.Count; j++)
                {
                    var matchB = matchesB[j];
                    IList<Cell> intersect = matchA.Cells.Intersect(matchB.Cells).ToList();
                    if (!intersect.Any())
                    {
                        continue;
                    }

                    var cells = matchA.Cells.Union(matchB.Cells).ToArray();
                    list.Add(_matchFactory.CreateMatch(cells, MatchShape.Cross, intersect[0].BoardPoint));
                    removeFromMatchesA.Add(i);
                    removeFromMatchesB.Add(j);
                }
            }
            
            foreach (var matchIndex in removeFromMatchesA.Reverse())
            {
                matchesA.RemoveAt(matchIndex);
            }

            foreach (var matchIndex in removeFromMatchesB.Reverse())
            {
                matchesB.RemoveAt(matchIndex);
            }

            return list;
        }


        public override Cell? RemoveCell(BoardPoint boardPoint)
        {
            var cell = this[boardPoint];
            if (cell == null)
            {
                return null;
            }

            _matchCache.Invalidate();
            var removedCell = new Cell(boardPoint, cell.Color, cell.SpecialEffectName);
            this[boardPoint] = null;
            return removedCell;
        }

        public override Solution FindSolution()
        {
            for (int i = 0; i < _columnSize - 1; i++)
            {
                for (int j = 0; j < _rowSize - 1; j++)
                {
                    var cellA = new BoardPoint(i, j);
                    var cellB = new BoardPoint(i + 1, j);
                    SwapCells(cellA, cellB);
                    if (FindMatchSequence().Any())
                    {
                        SwapCells(cellA, cellB);
                        return new Solution(cellA, cellB);
                    }

                    SwapCells(cellA, cellB);

                    var cellC = new BoardPoint(i, j + 1);
                    SwapCells(cellA, cellC);
                    if (FindMatchSequence().Any())
                    {
                        SwapCells(cellA, cellC);
                        return new Solution(cellA, cellC);
                    }

                    SwapCells(cellA, cellC);
                }
            }

            return null;
        }

        private CellState this[BoardPoint point]
        {
            get => _cells[point.Column, point.Row];
            set => _cells[point.Column, point.Row] = value;
        }

        public override IEnumerable<Cell> RemoveAllCells()
        {
            for (int row = 0; row < _rowSize; row++)
            {
                for (int column = 0; column < _columnSize; column++)
                {
                    var removedCell = RemoveCell(new BoardPoint(row, column));
                    if (removedCell != null)
                    {
                        yield return removedCell.Value;
                    }
                }
            }
        }

        public override bool IsSelectedCellsSwappable()
        {
            if (FirstSwapBoardPoint == null || SecondSwapBoardPoint == null)
            {
                return false;
            }

            var firstCell = FirstSwapBoardPoint.Value;
            var secondCell = SecondSwapBoardPoint.Value;

            return Mathf.Abs(firstCell.Row - secondCell.Row) == 1 && firstCell.Column == secondCell.Column ||
                   Mathf.Abs(firstCell.Column - secondCell.Column) == 1 && firstCell.Row == secondCell.Row;
        }

        private IEnumerable<BoardPoint> FindEmptyCells()
        {
            for (int column = 0; column < _columnSize; column++)
            {
                for (int row = 0; row < _rowSize; row++)
                {
                    if (_cells[column, row] == null)
                    {
                        yield return new BoardPoint(column, row);
                    }
                }
            }
        }

        private Match CreateMatch(IList<Cell> matchCells, MatchShape matchShape)
        {
            return _matchFactory.CreateMatch(
                matchCells,
                matchShape,
                FirstSwapBoardPoint,
                SecondSwapBoardPoint
            );
        }
    }
}