﻿using System.Collections.Generic;
using Core.Domain;
using UnityEngine;

namespace Core.Board.Model
{
    public abstract class AbstractBoardModel : MonoBehaviour 
    {
        public abstract BoardPoint? FirstSwapBoardPoint { get; set; }
        
        public abstract BoardPoint? SecondSwapBoardPoint { get; set; }

        public abstract IEnumerable<Cell> FillEmptyCells();
        
        public abstract IReadOnlyCollection<CellMovement> FindCellsDownMovement();
        
        public abstract void SwapSelectedCells();

        // returns matches in order of their removal 
        public abstract IList<IList<Match>> FindMatchSequence();
        
        public abstract Cell? RemoveCell(BoardPoint point);

        public abstract Solution FindSolution();

        public abstract IEnumerable<Cell> RemoveAllCells();
        
        public abstract bool IsSelectedCellsSwappable();
        
        public abstract void CreateCell(Cell cell);

        public abstract IEnumerable<Cell> FindSpecialCells();
        
        public abstract IEnumerable<Cell> GetRow(int row);
        
        public abstract IEnumerable<Cell> GetColumn(int column);
        
        public abstract IEnumerable<Cell> GetNeighborCells(BoardPoint boardPoint);
    }
}