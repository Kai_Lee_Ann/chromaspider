﻿using Core.Domain;

namespace Core.Board.Model
{
    internal class CellState
    {
        public CellColor Color { get; }

        public SpecialEffectName SpecialEffectName { get; }

        public CellState(Cell cell)
        {
            Color = cell.Color;
            SpecialEffectName = cell.SpecialEffectName;
        }

        public CellState(CellColor color, SpecialEffectName specialEffectName = SpecialEffectName.None)
        {
            Color = color;
            SpecialEffectName = specialEffectName;
        }

        public bool IsSpecial()
        {
            return SpecialEffectName != SpecialEffectName.None;
        }
    }
}