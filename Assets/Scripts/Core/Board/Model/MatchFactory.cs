﻿using System.Collections.Generic;
using System.Linq;
using Core.Board.Presenter.SpecialEffect;
using Core.Domain;

namespace Core.Board.Model
{
    public class MatchFactory
    {
        private readonly SpecialEffectFactory _specialEffectFactory;

        public MatchFactory(SpecialEffectFactory specialEffectFactory)
        {
            _specialEffectFactory = specialEffectFactory;
        }

        public Match CreateMatch(IList<Cell> matchCells, MatchShape matchShape, BoardPoint? centralBoardPoint = null)
        {
            var specialCell = CreateSpecialCell(matchCells, matchShape, centralBoardPoint);
            if (specialCell == null)
            {
                return new Match(matchCells);
            }
            // Debug.Log($"Special cell created: {specialCell.ToString()}");
            return new Match(matchCells, createdCell: specialCell);
        }
        
        public Match CreateMatch(
            IList<Cell> matchCells, 
            MatchShape matchShape, 
            BoardPoint? firstSelectedBoardPoint,
            BoardPoint? secondSelectedBoardPoint
        )
        {
            if (firstSelectedBoardPoint == null || secondSelectedBoardPoint == null)
            {
                return CreateMatch(matchCells, matchShape);
            }
            if (matchCells.Any(cell => cell.BoardPoint == firstSelectedBoardPoint))
            {
                return CreateMatch(matchCells, matchShape, firstSelectedBoardPoint);
            }
            if (matchCells.Any(cell => cell.BoardPoint == secondSelectedBoardPoint))
            {
                return CreateMatch(matchCells, matchShape, secondSelectedBoardPoint);
            }
            return CreateMatch(matchCells, matchShape);
        }
        
        private Cell? CreateSpecialCell(IList<Cell> cells, MatchShape shape, BoardPoint? specialCellBoardPoint = null)
        {
            var specialEffect = _specialEffectFactory.GetSpecialEffect(cells, shape);
            if (specialEffect == null)
            {
                return null;
            }
            return new Cell(
                specialCellBoardPoint ?? cells[cells.Count / 2].BoardPoint,
                cells[0].Color,
                specialEffect.Name
            );
        }
    }
}