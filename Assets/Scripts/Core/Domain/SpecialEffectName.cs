﻿namespace Core.Domain
{
    public enum SpecialEffectName
    {
        None,
        DestroyRow,
        DestroyColumn, 
        DestroyNeighbors
    }
}