﻿namespace Core.Domain
{
    public enum MatchShape
    {
        Column,
        Row,
        Cross
    }
}