﻿namespace Core.Domain
{
    public readonly struct CellMovement
    {
        public BoardPoint StartPoint { get; }
        public BoardPoint TargetPoint { get; }

        public CellMovement(BoardPoint startPoint, BoardPoint targetPoint)
        {
            StartPoint = startPoint;
            TargetPoint = targetPoint;
        }
    }
}