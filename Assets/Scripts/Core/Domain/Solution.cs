﻿namespace Core.Domain
{
    public class Solution
    {
        public BoardPoint FirstCell { get; }
        public BoardPoint SecondCell { get; }

        public Solution(BoardPoint firstCell, BoardPoint secondCell)
        {
            FirstCell = firstCell;
            SecondCell = secondCell;
        }
    }
}