﻿using System.Collections.Generic;

namespace Core.Domain
{
    public class Match
    {
        public IReadOnlyList<Cell> Cells { get; }

        public CellColor Color { get; }

        public Cell? SpecialCell { get; }

        public Cell? CreatedCell { get; }

        public Match(IList<Cell> cells, Cell? createdCell = null, Cell? specialCell = null)
        {
            Cells = new List<Cell>(cells);
            CreatedCell = createdCell;
            SpecialCell = specialCell;
            Color = specialCell?.Color ?? cells[0].Color;
        }

        public override string ToString()
        {
            return SpecialCell == null
                ? $"Match(Color={Color}, Cells.Count={Cells.Count}, CreateCell={CreatedCell != null})"
                : $"Match(Color={Color}, Cells.Count={Cells.Count}, CreateCell={CreatedCell != null}, SpecialEffect={SpecialCell.Value.SpecialEffectName})";
        }
    }
}