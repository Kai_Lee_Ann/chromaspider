﻿using Core.Board.Model;

namespace Core.Domain
{
    public readonly struct Cell
    {
        public BoardPoint BoardPoint { get; }

        public CellColor Color { get; }

        public SpecialEffectName SpecialEffectName { get; }
        
        public bool IsSpecial()
        {
            return SpecialEffectName != SpecialEffectName.None;
        } 

        public Cell(BoardPoint boardPoint, CellColor color, SpecialEffectName specialEffectName = SpecialEffectName.None)
        {
            BoardPoint = boardPoint;
            Color = color;
            SpecialEffectName = specialEffectName;
        }

        internal Cell(int column, int row, CellState cellState) : 
            this(new BoardPoint(column, row), cellState.Color, cellState.SpecialEffectName)
        {
        }
        
        internal Cell(BoardPoint boardPoint, CellState cellState) : 
            this(boardPoint, cellState.Color, cellState.SpecialEffectName)
        {
        }

        public bool Equals(Cell other)
        {
            return BoardPoint.Equals(other.BoardPoint) && Color.Equals(other.Color) &&
                   SpecialEffectName.Equals(other.SpecialEffectName);
        }

        public override bool Equals(object obj)
        {
            return obj is Cell other && Equals(other);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = BoardPoint.GetHashCode();
                hashCode = (hashCode * 397) ^ (int) Color;
                hashCode = (hashCode * 397) ^ (int) SpecialEffectName;
                return hashCode;
            }
        }

        public override string ToString()
        {
            return $"Cell(BoardPoint={BoardPoint}, Color={Color}, SpecialEffectName={SpecialEffectName})";
        }
    }
}