﻿namespace Core.Domain
{
    public class Score
    {
        public int Amount { get; }

        public Match Match { get; }

        public Score(int amount, Match match)
        {
            Amount = amount;
            Match = match;
        }
    }
}