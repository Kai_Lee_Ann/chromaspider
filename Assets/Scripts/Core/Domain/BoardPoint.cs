﻿namespace Core.Domain
{
    public readonly struct BoardPoint
    {
        public int Column { get; }
        public int Row { get; }

        public BoardPoint(int column, int row)
        {
            Row = row;
            Column = column;
        }

        public override string ToString()
        {
            return $"(row={Row.ToString()}, column={Column.ToString()})";
        }

        public bool Equals(BoardPoint other)
        {
            return Column == other.Column && Row == other.Row;
        }

        public override bool Equals(object obj)
        {
            return obj is BoardPoint other && Equals(other);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Column * 397) ^ Row;
            }
        }

        public static bool operator ==(BoardPoint pointA, BoardPoint pointB)
        {
            return Equals(pointA, pointB);
        }

        public static bool operator !=(BoardPoint pointA, BoardPoint pointB)
        {
            return !(pointA == pointB);
        }
    }
}