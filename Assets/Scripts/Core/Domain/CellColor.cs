﻿using System;
using UnityEngine;

namespace Core.Domain
{
    public enum CellColor
    {
        Aqua,
        Yellow,
        Green,
        Red,
        Sand,
        Violet
    }
    
    internal static class ColorMapper
    {
        public static readonly Color Aqua = new Color(0.25f, 0.99f, 1f);
        public static readonly Color Yellow = new Color(0.95f, 0.86f, 0.0f);
        public static readonly Color Green = new Color(0.231f, 0.988f, 0.2f, 1f);
        public static readonly Color Red = new Color(0.98f, 0.1f, 0.05f);
        public static readonly Color Sand = new Color(0.98f, 0.85f, 0.54f);
        public static readonly Color Violet = new Color(0.85f, 0.31f, 1f);

        public static Color GetColor(this CellColor color)
        {
            switch (color)
            {
                case CellColor.Aqua:
                    return Aqua;
                case CellColor.Yellow:
                    return Yellow;
                case CellColor.Green:
                    return Green;
                case CellColor.Red:
                    return Red;
                case CellColor.Sand:
                    return Sand;
                case CellColor.Violet:
                    return Violet;
                default:
                    throw new ArgumentOutOfRangeException(nameof(color), color, null);
            }
        }
    }

}