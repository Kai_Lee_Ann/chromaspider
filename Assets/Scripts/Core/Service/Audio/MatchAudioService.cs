﻿using System;
using Core.Service.Event;
using UnityEngine;

namespace Core.Service.Audio
{
    [RequireComponent(typeof(AudioSource))]
    public class MatchAudioService : MonoBehaviour
    {
        private static readonly int[] PentatonicScaleSemitones = {2, 2, 2, 3, 3};
        private const float SemitonePitchMultiplier = 1.05946f;

        public EventMessenger eventMessenger;
        public AudioClip matchSound;
        private AudioSource _audioSource;

        public void Awake()
        {
            _audioSource = GetComponent<AudioSource>();
            eventMessenger.AddListener(
                GameEvent.TotalScoreUpdated,
                (Action<int, int>) ((score, combo) => PlayMatchSound(combo))
            );
        }

        private void PlayMatchSound(int step)
        {
            if (step % PentatonicScaleSemitones.Length == 0)
            {
                _audioSource.pitch = 1;
            }
            else
            {
                int semitones = PentatonicScaleSemitones[step % PentatonicScaleSemitones.Length];
                _audioSource.pitch *= Mathf.Pow(SemitonePitchMultiplier, semitones);
            }
            _audioSource.PlayOneShot(matchSound);
        }
    }
}