﻿using UnityEngine;

namespace Core.Service.Audio
{
    [RequireComponent(typeof(AudioSource))]
    public class MusicService : MonoBehaviour
    {
        private const float DefaultVolume = 0.5f;
        private AudioSource _audioSource;

        private void Awake()
        {
            _audioSource = GetComponent<AudioSource>();
            _audioSource.volume = DefaultVolume;
        }
        
        public void Enable(bool enableMusic)
        {
            if (enableMusic && _audioSource.isPlaying)
            {
                return;
            }
            if (enableMusic)
            {
                _audioSource.Play();
                return;
            }
            _audioSource.Stop();
            // _audioSource.volume = enableMusic ? DefaultVolume : 0f;
        }
    }
}