﻿using UnityEngine;

namespace Core.Service.Audio
{
    [RequireComponent(typeof(AudioSource))]
    public class Singleton : MonoBehaviour 
    {
        private static GameObject _instance;

        private void Awake()
        {
            var currentInstance = gameObject;
            if (_instance != null)
            {
                Destroy(currentInstance);
                return;
            }
            _instance = currentInstance;
            DontDestroyOnLoad(currentInstance);
        }
    }
}