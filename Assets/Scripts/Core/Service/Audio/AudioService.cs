﻿using System;
using Core.Domain;
using Core.Service.Event;
using UnityEngine;

namespace Core.Service.Audio
{
    [RequireComponent(typeof(AudioSource))]
    public class AudioService : MonoBehaviour
    {
        public EventMessenger eventMessenger;
        public AudioClip startSound;
        public AudioClip fallDownSound;
        public AudioClip highComboSound;
        public AudioClip dramaticComboSound;
        public AudioClip specialCellCreatedSound;
        public AudioClip specialCellActivatedSound;
        public AudioClip winSound;
        public AudioClip looseSound;
        public AudioClip swapSound;

        private AudioSource _audioSource;

        public void Awake()
        {
            _audioSource = GetComponent<AudioSource>();
            eventMessenger.AddListener(GameEvent.GameStarted, new Action(() => PlayOneShot(startSound)));
            eventMessenger.AddListener(
                GameEvent.TotalScoreUpdated,
                (Action<int, int>) ((score, combo) => PlayHighComboSound(combo))
            );
            eventMessenger.AddListener(
                GameEvent.TotalScoreUpdated,
                (Action<int, int>) ((score, combo) => PlayDramaticComboSound(combo))
            );
            eventMessenger.AddListener(GameEvent.MoveDownStart, new Action(PlayFallDownSound));
            eventMessenger.AddListener(
                GameEvent.SpecialCellCreated,
                new Action(() => PlayOneShot(specialCellCreatedSound))
            );
            eventMessenger.AddListener(
                GameEvent.SpecialCellActivated,
                new Action<SpecialEffectName>(PlaySpecialEffectSound)
            );
            eventMessenger.AddListener(
                GameEvent.GameWon,
                new Action(() => PlayOneShot(winSound))
            );
            eventMessenger.AddListener(
                GameEvent.GameWonNewRecord,
                new Action(() => PlayOneShot(winSound))
            );
            eventMessenger.AddListener(
                GameEvent.GameLost,
                new Action(() => PlayOneShot(looseSound))
            );
            eventMessenger.AddListener(
                GameEvent.SwapMade,
                new Action(() => PlayOneShot(swapSound))
            );
        }

        private void PlaySpecialEffectSound(SpecialEffectName effectName)
        {
            switch (effectName)
            {
                case SpecialEffectName.DestroyRow:
                case SpecialEffectName.DestroyColumn:
                case SpecialEffectName.DestroyNeighbors:
                    PlayOneShot(specialCellActivatedSound);
                    break;
                case SpecialEffectName.None:
                    throw new ArgumentException($"Shouldn't be called with {effectName}");
                default:
                    throw new ArgumentOutOfRangeException(nameof(effectName), effectName, null);
            }
        }

        private void PlayFallDownSound()
        {
            _audioSource.clip = fallDownSound;
            _audioSource.PlayDelayed(0.1f);
        }

        private void PlayDramaticComboSound(int combo)
        {
            if (combo == 4)
            {
                PlayOneShot(dramaticComboSound);
            }
        }

        private void PlayHighComboSound(int combo)
        {
            if (combo >= 2 && combo % 2 == 0)
            {
                PlayOneShot(highComboSound);
            }
        }

        private void PlayOneShot(AudioClip audioClip)
        {
            _audioSource.PlayOneShot(audioClip);
        }
    }
}