﻿namespace Core.Service.Event
{
    public enum GameEvent
    {
        GameStarted,
        BoardReset,
        MatchClearStart,
        AllMatchesCleared,
        ScoreAdded,
        TotalScoreUpdated,
        SwapMade,
        TurnMade,
        TurnsUpdated,
        NewTurnStart,
        GameWon,
        GameLost,
        MoveDownStart,
        SpecialCellCreated,
        SpecialCellActivated,
        GameWonNewRecord,
        GameUnpaused,
        GamePaused
    }
}