﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Core.Service.Event
{
    public class EventMessenger : MonoBehaviour
    {
        private readonly Dictionary<GameEvent, Delegate> _actionTable = new Dictionary<GameEvent, Delegate>();

        public void AddListener(GameEvent eventType, Delegate listener)
        {
            if (!_actionTable.TryGetValue(eventType, out Delegate action))
            {
                _actionTable[eventType] = listener;
                return;
            }

            if (action.GetType() != listener.GetType())
            {
                throw new ArgumentException(
                    $"Invalid delegate type! Expected: {action.GetType()}, actual: {listener.GetType()}"
                );
            }

            _actionTable[eventType] = Delegate.Combine(action, listener);
        }

        public void SendMessage(GameEvent @event)
        {
            // Debug.Log($"Message Sent: {@event}");
            foreach (var action in GetInvocationList<Action>(@event))
            {
                action.Invoke();
            }
        }


        public void SendMessage<T>(GameEvent @event, T param)
        {
            // Debug.Log($"Message Sent: {@event}, {param}");
            foreach (var action in GetInvocationList<Action<T>>(@event))
            {
                action.Invoke(param);
            }
        }
        
        public void SendMessage<T1, T2>(GameEvent @event, T1 paramA, T2 paramB)
        {
            // Debug.Log($"Message Sent: {@event}, {paramA}, {paramB}");
            foreach (var action in GetInvocationList<Action<T1, T2>>(@event))
            {
                action.Invoke(paramA, paramB);
            }
        }

        private T[] GetInvocationList<T>(GameEvent @event)
        {
            return _actionTable.TryGetValue(@event, out Delegate callback)
                ? callback.GetInvocationList().Cast<T>().ToArray()
                : new T[0];
        }

        private void OnDestroy()
        {
            _actionTable.Clear();
        }
    }
}