﻿using System;
using Core.Service.Event;
using Core.Settings;
using TMPro;
using UnityEngine;

namespace Core.Service.UI.Label
{
    public class EndlessGameLabelService : MonoBehaviour
    {
        public TextMeshProUGUI scoreText;
        public EventMessenger eventMessenger;
        public GameSettings gameSettings;

        public void Awake()
        {
            UpdateScoreLabel(gameSettings.startingScore);
            eventMessenger.AddListener(
                GameEvent.TotalScoreUpdated,
                (Action<int, int>) ((score, combo) => UpdateScoreLabel(score))
            );
        }

        private void UpdateScoreLabel(int scoreValue)
        {
            scoreText.text = "Score: " + scoreValue.ToString("D5");
        }
    }
}