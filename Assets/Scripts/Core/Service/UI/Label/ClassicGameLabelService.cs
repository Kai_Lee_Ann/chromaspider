﻿using System;
using Core.Domain;
using Core.Service.Event;
using Core.Settings;
using TMPro;
using UnityEngine;

namespace Core.Service.UI.Label
{
    public class ClassicGameLabelService : MonoBehaviour
    {
        public TextMeshProUGUI scoreText;
        public TextMeshProUGUI turnsText;
        public TextMeshProUGUI targetText;
        public TextMeshProUGUI winConditionPanelText;

        public EventMessenger eventMessenger;
        public GameSettings gameSettings;

        public void Awake()
        {
            UpdateScoreLabel(gameSettings.startingScore);
            UpdateTurnsLeft(gameSettings.turns);
            UpdateWinConditionText(gameSettings.targetScore, gameSettings.turns);

            eventMessenger.AddListener(GameEvent.TurnsUpdated, (Action<int>) UpdateTurnsLeft);
            eventMessenger.AddListener(
                GameEvent.TotalScoreUpdated,
                (Action<int, int>) ((score, combo) => UpdateScoreLabel(score))
            );
        }

        private void UpdateScoreLabel(int scoreValue)
        {
            scoreText.text = "Score: " + scoreValue.ToString("D5");
        }

        private void UpdateTurnsLeft(int turnsLeft)
        {
            turnsText.text = "Turns: " + turnsLeft.ToString("D2");
            if (turnsLeft <= 3)
            {
                turnsText.color = CellColor.Red.GetColor();
            }
        }

        private void UpdateWinConditionText(int targetScore, int turnsLeft)
        {
            targetText.text = "Target: " + targetScore.ToString("D5");
            winConditionPanelText.text = $"Score {targetScore} points in {turnsLeft} turns!";
        }
    }
}