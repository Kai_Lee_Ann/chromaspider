﻿using System;
using Core.Service.Event;
using UnityEngine;

namespace Core.Service.UI
{
    public class DialogWindowService : MonoBehaviour
    {
        [SerializeField] private GameObject gameOverPanel;
        [SerializeField] private GameObject winPanel;
        [SerializeField] private GameObject newRecordWinPanel;

        public EventMessenger eventMessenger;
        
        public void Awake()
        {
            winPanel.SetActive(false);
            eventMessenger.AddListener(GameEvent.GameWon, new Action(() => winPanel.SetActive(true)));
            eventMessenger.AddListener(GameEvent.GameWonNewRecord, new Action(() => newRecordWinPanel.SetActive(true)));
            eventMessenger.AddListener(GameEvent.GameLost, new Action(() => gameOverPanel.SetActive(true)));
        }
    }
    
    
}