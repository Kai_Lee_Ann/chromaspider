﻿using System;
using Core.Service.Event;
using Core.Settings;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Service.UI
{
    [RequireComponent(typeof(Slider))]
    public class ScoreProgressBar : MonoBehaviour
    {
        public EventMessenger eventMessenger;
        public GameSettings gameSettings;

        private Slider _slider;
        private int _targetScore;

        private void Awake()
        {
            _slider = GetComponent<Slider>();
            _targetScore = gameSettings.targetScore;
            UpdateSliderValue(0);
            eventMessenger.AddListener(
                GameEvent.TotalScoreUpdated,
                (Action<int, int>) ((score, combo) => UpdateSliderValue(score))
            );
        }

        private void UpdateSliderValue(int score)
        {
            _slider.value = score == 0 ? 0 : score / (float) _targetScore;
        }
    }
}