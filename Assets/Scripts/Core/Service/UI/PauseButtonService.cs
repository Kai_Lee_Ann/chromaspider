﻿using System;
using Core.Service.Event;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Service.UI
{
    [RequireComponent(typeof(Button))]
    public class PauseButtonService : MonoBehaviour
    {
        public EventMessenger eventMessenger;
        private Button _button;

        private void Awake()
        {
            _button = GetComponent<Button>();
            eventMessenger.AddListener(GameEvent.GameUnpaused, new Action(() => _button.interactable = true));
            eventMessenger.AddListener(GameEvent.GamePaused, new Action(() => _button.interactable = false));
        }
    }
}