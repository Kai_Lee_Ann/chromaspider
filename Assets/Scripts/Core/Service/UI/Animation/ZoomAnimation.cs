﻿using UnityEngine;

namespace Core.Service.UI.Animation
{
    public class ZoomAnimation : MonoBehaviour
    {
        private const float ZoomScale = 1.0175f;
        public float speed;

        private void Update()
        {
            gameObject.transform.localScale = new Vector3(
                Mathf.Lerp(1, ZoomScale, Mathf.PingPong(Time.time * speed, 1)),
                Mathf.Lerp(1, ZoomScale, Mathf.PingPong(Time.time * speed, 1)),
                1
            );
        }
    }
}