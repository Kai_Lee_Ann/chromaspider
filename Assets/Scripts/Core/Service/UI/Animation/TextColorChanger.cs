﻿using Core.Domain;
using TMPro;
using UnityEngine;

namespace Core.Service.UI.Animation
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class TextColorChanger : MonoBehaviour
    {
        public float lerpChangeTime;
        private static readonly Color[] Colors =
        {
            ColorMapper.Aqua,
            ColorMapper.Violet,
            ColorMapper.Green,
            ColorMapper.Yellow,
            ColorMapper.Sand,
            ColorMapper.Red,
        };

        
        private float _elapsedTime;
        private Color _currentColor;
        private Color _nextColor;
        private int _currentColorIndex;
        private TextMeshProUGUI _text;

        private void Awake()
        {
            _text = GetComponent<TextMeshProUGUI>();
            _currentColor = Colors[_currentColorIndex % Colors.Length];
            _nextColor = Colors[_currentColorIndex + 1 % Colors.Length];
        }

        private void Update()
        {
            _elapsedTime += Time.deltaTime;
            _text.color = Color.Lerp(_currentColor, _nextColor, _elapsedTime / lerpChangeTime);
            if (_elapsedTime <= lerpChangeTime)
            {
                return;
            }
            _elapsedTime = 0f;
            _currentColorIndex++;
            _currentColor = Colors[_currentColorIndex % Colors.Length];
            _nextColor = Colors[(_currentColorIndex + 1) % Colors.Length];
        }
    }
}