﻿using UnityEngine;
using UnityEngine.UI;

namespace Core.Service.UI.Animation
{
    [RequireComponent(typeof(Image))]
    public class ImageBreathAnimation : MonoBehaviour
    {
        private const float TransitionTime = 1f;
        private Image _image;
        private Color _imageColor;
        private Color _transparentColor;

        private void Awake()
        {
            _image = GetComponent<Image>();
            _imageColor = _image.color;
            _transparentColor = new Color(_imageColor.r, _imageColor.g, _imageColor.b, 0f);
        }

        private void Update()
        {
            _image.color = Color.Lerp(_imageColor, _transparentColor, Mathf.PingPong(Time.time, TransitionTime));
        }
    }
}