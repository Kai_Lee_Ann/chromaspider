﻿using Core.Domain;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Service.UI.Animation
{
    [RequireComponent(typeof(Image))]
    public class ImageColorChanger : MonoBehaviour
    {
        public float lerpChangeTime;
        private static readonly Color[] Colors =
        {
            ColorMapper.Aqua,
            ColorMapper.Violet,
            ColorMapper.Green,
            ColorMapper.Yellow,
            ColorMapper.Sand,
            ColorMapper.Red,
        };

        
        private float _elapsedTime;
        private Color _currentColor;
        private Color _nextColor;
        private int _currentColorIndex;
        private Image _image;

        private void Awake()
        {
            _image = GetComponent<Image>();
            _currentColor = Colors[_currentColorIndex % Colors.Length];
            _nextColor = Colors[_currentColorIndex + 1 % Colors.Length];
        }

        private void Update()
        {
            _elapsedTime += Time.deltaTime;
            _image.color = Color.Lerp(_currentColor, _nextColor, _elapsedTime / lerpChangeTime);
            if (_elapsedTime <= lerpChangeTime)
            {
                return;
            }
            _elapsedTime = 0f;
            _currentColorIndex++;
            _currentColor = Colors[_currentColorIndex % Colors.Length];
            _nextColor = Colors[(_currentColorIndex + 1) % Colors.Length];
        }
    }
}