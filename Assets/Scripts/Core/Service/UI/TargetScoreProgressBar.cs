﻿using System;
using Core.Service.Event;
using Core.Settings;
using UnityEngine;
using UnityEngine.UI;

namespace Core.Service.UI
{
    [RequireComponent(typeof(Slider))]
    public class TargetScoreProgressBar : MonoBehaviour
    {
        public EventMessenger eventMessenger;
        public GameSettings gameSettings;

        private Slider _slider;
        private int _turnsTotal;

        private void Awake()
        {
            _slider = GetComponent<Slider>();
            _turnsTotal = gameSettings.turns;
            UpdateSliderValue(_turnsTotal);
            eventMessenger.AddListener(GameEvent.TurnsUpdated, (Action<int>) UpdateSliderValue);
        }

        private void UpdateSliderValue(int turnsLeft)
        {
            _slider.value = turnsLeft == _turnsTotal ? 0 : 1f - turnsLeft / (float) _turnsTotal;
        }
    }
}