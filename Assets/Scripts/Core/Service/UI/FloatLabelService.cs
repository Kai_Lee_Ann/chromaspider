﻿using System;
using System.Collections;
using System.Collections.Generic;
using Core.Board.View;
using Core.Domain;
using Core.Service.Event;
using Core.Settings;
using TMPro;
using UnityEngine;

namespace Core.Service.UI
{
    public class FloatLabelService : MonoBehaviour
    {
        private const string FloatUpAnimation = "FloatUp";
        private const string NoTurnsFound = "No Turns Found";
        private const float AnimationTime = 1.5f;

        public GameSettings gameSettings;
        public GameObject scoreLabelPrefab;
        public EventMessenger eventMessenger;
        public AbstractBoardView view;

        private GameObject[] _scoreLabels;
        private TextMeshProUGUI[] _texts;
        private Animator[] _animators;
        private RectTransform[] _rectTransforms;
        private Queue<int> _freeScoreLabels;
        private Camera _camera;

        private void Start()
        {
            Debug.Assert(Camera.main != null, "Camera.main != null");
            _camera = Camera.main;
        }

        private void Awake()
        {
            int scoreLabelPoolSize = GetPoolSize(); 
            _scoreLabels = new GameObject[scoreLabelPoolSize];
            _texts = new TextMeshProUGUI[scoreLabelPoolSize];
            _animators = new Animator[scoreLabelPoolSize];
            _rectTransforms = new RectTransform[scoreLabelPoolSize];
            _freeScoreLabels = new Queue<int>(scoreLabelPoolSize);

            for (int i = 0; i < scoreLabelPoolSize; i++)
            {
                GameObject scoreLabel = Instantiate(scoreLabelPrefab, gameObject.transform);
                scoreLabel.SetActive(false);
                TextMeshProUGUI text = scoreLabel.GetComponent<TextMeshProUGUI>();
                _rectTransforms[i] = scoreLabel.GetComponent<RectTransform>();
                _scoreLabels[i] = scoreLabel;
                _texts[i] = text;
                _animators[i] = scoreLabel.GetComponent<Animator>();
                _freeScoreLabels.Enqueue(i);
            }

            eventMessenger.AddListener(GameEvent.BoardReset, new Action(DrawNoTurnsFound));
            eventMessenger.AddListener(GameEvent.ScoreAdded, new Action<Score>(DrawScoreLabel));
        }

        private int GetPoolSize()
        {
            return 2 * gameSettings.columnSize * gameSettings.rowSize / gameSettings.minMatchLength;
        }

        private void DrawNoTurnsFound()
        {
            DrawFloatLabel(Vector3.zero, NoTurnsFound, Color.white);
        }

        private void DrawScoreLabel(Score score)
        {
            var match = score.Match;
            DrawFloatLabel(GetScoreDrawPosition(match), score.Amount.ToString(), match.Color.GetColor());
        }

        private Vector2 GetScoreDrawPosition(Match match)
        {
            var cells = match.Cells;
            if (match.SpecialCell != null)
            {
                return view.BoardPointToWorldPoint(match.SpecialCell.Value.BoardPoint);
            }
            return view.BoardPointToWorldPoint(cells[cells.Count / 2].BoardPoint);
        }

        private void DrawFloatLabel(Vector3 position, string text, Color textColor)
        {
            int index = _freeScoreLabels.Dequeue();
            GameObject scoreLabel = _scoreLabels[index];
            scoreLabel.SetActive(true);
            _rectTransforms[index].position = _camera.WorldToScreenPoint(position);
            TextMeshProUGUI textMesh = _texts[index];
            textMesh.text = text;
            textMesh.color = textColor;
            Animator animator = _animators[index];
            animator.enabled = true;
            animator.Play(FloatUpAnimation);

            StartCoroutine(Release(scoreLabel));
        }

        private IEnumerator Release(GameObject scoreLabel)
        {
            yield return new WaitForSeconds(AnimationTime);
            scoreLabel.SetActive(false);
            int index = Array.FindIndex(_scoreLabels, o => ReferenceEquals(o, scoreLabel));
            Animator animator = _animators[index];
            animator.WriteDefaultValues();
            animator.enabled = false;
            _freeScoreLabels.Enqueue(index);
        }
    }
}