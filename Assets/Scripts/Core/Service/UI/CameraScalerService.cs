﻿using Core.Settings;
using UnityEngine;

namespace Core.Service.UI
{
    [RequireComponent(typeof(Camera))]
    public class CameraScalerService : MonoBehaviour
    {
        public GameSettings gameSettings;
        public int horizontalScaleModifier;

        private void Awake()
        {
            Camera mainCamera = GetComponent<Camera>();
            if (horizontalScaleModifier == 0)
            {
                horizontalScaleModifier = 4;
            }
            if (mainCamera.aspect < 1) // vertical
            {
                mainCamera.orthographicSize = gameSettings.columnSize / (mainCamera.aspect * 2);
            }
            else
            {
                mainCamera.orthographicSize = (gameSettings.rowSize + horizontalScaleModifier) / 2f;
            }
        }
    }
}
