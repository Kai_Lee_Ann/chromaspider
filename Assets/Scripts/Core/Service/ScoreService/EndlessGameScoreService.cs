﻿using System;
using System.Collections.Generic;
using Core.Domain;
using Core.Service.Event;
using Core.Settings;


namespace Core.Service.ScoreService
{
    public class EndlessGameScoreService : AbstractScoreService
    {
        public GameSettings gameSettings;
        
        public EventMessenger eventMessenger;
        public override bool IsGameStarting { get; protected set; }
        public override int TotalScore { get; protected set; }
        public override int TurnsLeft { get; protected set; }
        private int _totalScore;

        private void Start()
        {
            cellScorePrice = gameSettings.cellScorePrice;
            maxCombo = gameSettings.maxCombo;

            eventMessenger.AddListener(GameEvent.GameStarted, new Action(() => { IsGameStarting = true; }));
            eventMessenger.AddListener(GameEvent.MatchClearStart, new Action<IEnumerable<Match>>(AddScore));
            eventMessenger.AddListener(GameEvent.NewTurnStart, new Action(OnTurnStart));
            eventMessenger.AddListener(GameEvent.AllMatchesCleared,
                new Action(() =>
                {
                    eventMessenger.SendMessage(GameEvent.TotalScoreUpdated, _totalScore, combo);
                    IncrementCombo();
                })
            );
        }

        private void OnTurnStart()
        {
            IsGameStarting = false;
            combo = 0;
        }

        private void AddScore(IEnumerable<Match> matches)
        {
            if (IsGameStarting)
            {
                return;
            }

            foreach (var match in matches)
            {
                var score = CalculateScore(match);
                _totalScore += score;
                // Debug.Log($"ScoreService.AddScore: score={score.ToString()}, match={match}");
                eventMessenger.SendMessage(GameEvent.ScoreAdded, new Score(score, match));
            }
        }

        private void IncrementCombo()
        {
            combo++;
        }
    }
}