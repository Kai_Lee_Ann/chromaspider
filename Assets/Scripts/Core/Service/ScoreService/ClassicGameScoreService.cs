﻿using System;
using System.Collections.Generic;
using Core.Domain;
using Core.Service.Event;
using Core.Settings;
using Persistence;

namespace Core.Service.ScoreService
{
    public class ClassicGameScoreService : AbstractScoreService
    {
        public GameSettings gameSettings;
        public EventMessenger eventMessenger;
        public LeaderboardData leaderboardData;
        public override bool IsGameStarting { get; protected set; }
        public override int TotalScore { get; protected set; }
        public override int TurnsLeft { get; protected set; }

        private int _targetScore;

        private void Start()
        {
            TurnsLeft = gameSettings.turns;
            _targetScore = gameSettings.targetScore;
            cellScorePrice = gameSettings.cellScorePrice;
            maxCombo = gameSettings.maxCombo;

            eventMessenger.AddListener(GameEvent.GameStarted, new Action(() => { IsGameStarting = true; }));
            eventMessenger.AddListener(GameEvent.MatchClearStart, new Action<IEnumerable<Match>>(AddScore));
            eventMessenger.AddListener(GameEvent.NewTurnStart, new Action(OnTurnStart));
            eventMessenger.AddListener(GameEvent.TurnMade, new Action(DecrementTurns));
            eventMessenger.AddListener(GameEvent.AllMatchesCleared,
                new Action(() =>
                {
                    eventMessenger.SendMessage(GameEvent.TotalScoreUpdated, TotalScore, combo);
                    IncrementCombo();
                })
            );
        }

        private void OnTurnStart()
        {
            if (!CheckWin())
            {
                CheckLoose();
            }

            IsGameStarting = false;
            combo = 0;
        }

        private void AddScore(IEnumerable<Match> matches)
        {
            if (IsGameStarting)
            {
                return;
            }

            foreach (var match in matches)
            {
                var score = CalculateScore(match);
                TotalScore += score;
                // Debug.Log($"ScoreService.AddScore: score={score.ToString()}, match={match}");
                eventMessenger.SendMessage(GameEvent.ScoreAdded, new Score(score, match));
            }
        }

        private void IncrementCombo()
        {
            combo++;
        }

        private void DecrementTurns()
        {
            TurnsLeft--;
            eventMessenger.SendMessage(GameEvent.TurnsUpdated, TurnsLeft);
        }

        private bool CheckWin()
        {
            if (TotalScore < _targetScore)
            {
                return false;
            }

            eventMessenger.SendMessage(
                leaderboardData.IsNewRecord(gameSettings.turns - TurnsLeft, TotalScore)
                    ? GameEvent.GameWonNewRecord
                    : GameEvent.GameWon
            );

            return true;
        }

        private void CheckLoose()
        {
            if (TurnsLeft > 0)
            {
                return;
            }

            eventMessenger.SendMessage(GameEvent.GameLost);
        }
    }
}