﻿using Core.Settings;
using Persistence;
using TMPro;
using UnityEngine;

namespace Core.Service.ScoreService
{
    public class LeaderBoardUpdateService : MonoBehaviour
    {
        public AbstractScoreService gameScoreService;
        public GameSettings gameSettings;
        public LeaderboardData leaderboardData;
        public TMP_InputField inputField;

        public void WriteRecord()
        {
            var inputFieldText = inputField.text;
            leaderboardData.AddEntry(
                inputFieldText == null ? "" : inputFieldText.Trim(),
                gameSettings.turns - gameScoreService.TurnsLeft,
                gameScoreService.TotalScore
            );
        }
    }
}