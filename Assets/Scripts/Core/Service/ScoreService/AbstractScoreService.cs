﻿using System;
using Core.Domain;
using UnityEngine;

namespace Core.Service.ScoreService
{
    public abstract class AbstractScoreService : MonoBehaviour
    {
        protected int cellScorePrice;
        protected int combo;
        protected int maxCombo;

        public abstract bool IsGameStarting { get; protected set; }
        public abstract int TotalScore { get; protected set; }
        public abstract int TurnsLeft { get; protected set; }

        protected int CalculateScore(Match match)
        {
            var count = match.Cells.Count;
            var comboScore = cellScorePrice / 2 * Math.Min(combo, maxCombo);
            switch (match.SpecialCell?.SpecialEffectName)
            {
                case SpecialEffectName.DestroyRow:
                    return 8 * cellScorePrice + comboScore;
                case SpecialEffectName.DestroyColumn:
                    return 8 * cellScorePrice + comboScore;
                case SpecialEffectName.DestroyNeighbors:
                    return 10 * cellScorePrice + comboScore;
                default:
                    return count * cellScorePrice + comboScore;
            }
        }
    }
}