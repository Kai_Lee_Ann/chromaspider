﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Util
{
    [CreateAssetMenu(fileName = "SceneService", menuName = "Custom Scriptable Objects/Scene Service")]
    public class SceneService : ScriptableObject
    {
        public static void LoadScene(string name)
        {
            SceneManager.LoadScene(name);
        }

        public static void UnloadSceneAsync(string name)
        {
            SceneManager.UnloadSceneAsync(name);
        }

        public static void LoadSceneAdditive(string name)
        {
            SceneManager.LoadScene(name, LoadSceneMode.Additive);
        }

        public static void QuitGame()
        {
            Application.Quit();
        }
    }
}