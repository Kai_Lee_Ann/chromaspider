﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Util
{
    public class CoroutineExecutor : MonoBehaviour
    {
        public void Execute(Task task)
        {
            foreach (var coroutine in task.Coroutines)
            {
                if (coroutine.Delay == 0)
                {
                    StartCoroutine(coroutine);
                    continue;
                }
                StartCoroutine(coroutine.GetDelayed());
            }
        }

        public class Task
        {
            public static TaskBuilder Builder => new TaskBuilder();
            public IList<Coroutine> Coroutines { get; }

            public bool Finished => Coroutines.All(coroutine => coroutine.Finished);


            private Task(IList<Coroutine> coroutines)
            {
                Coroutines = coroutines;
            }


            public class TaskBuilder
            {
                private readonly IList<Coroutine> _coroutines;

                public TaskBuilder()
                {
                    _coroutines = new List<Coroutine>();
                }

                public void Add(IEnumerator enumerable, float delay = 0)
                {
                    _coroutines.Add(new Coroutine(enumerable, delay));
                }

                public Task Build()
                {
                    return new Task(_coroutines);
                }
            }
        }

        public class Coroutine : IEnumerator
        {
            public float Delay { get; }
            public bool Finished { get; private set; }

            private readonly IEnumerator _coroutine;

            public object Current => _coroutine.Current;

            public Coroutine(IEnumerator coroutine, float delay = 0)
            {
                _coroutine = coroutine;
                Delay = delay;
            }

            public bool MoveNext()
            {
                var moveNext = _coroutine.MoveNext();
                if (!moveNext)
                {
                    Finished = true;
                }
                return moveNext;
            }

            public void Reset()
            {
                _coroutine.Reset();
                Finished = false;
            }

            public IEnumerator GetDelayed()
            {
                yield return new WaitForSeconds(Delay);
                yield return this;
            }
        }
    }
}